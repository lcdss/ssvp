<?php

namespace App\Http\Controllers;

class AdminController extends Controller
{
    public function index()
    {
        if (\Auth::user()->hasRole(['owner', 'user']))
            return redirect()->route('admin.post.index');

        return view('admin.index');
    }
}
