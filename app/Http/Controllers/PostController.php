<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Post;
use App\Repositories\ChannelRepository;
use App\Repositories\PostRepository;

class PostController extends Controller
{
    private $post;

    public function __construct(PostRepository $post)
    {
        $this->post = $post;
    }

    public function index()
    {
        $posts = $this->post->allWithPaginate(['channel', 'user']);

        return view('admin.posts.index', compact('posts'));
    }

    public function create(ChannelRepository $channel)
    {
        $channels = $channel->lists('title', 'id');

        return view('admin.posts.create', compact('channels'));
    }

    public function store(CreatePostRequest $request)
    {
        $data = $request->except('_token');

        $post = $this->post->create($data);

        flash()->success('Post criado com sucesso.');

        return redirect()->route('admin.post.index');
    }

    public function edit(Post $post, ChannelRepository $channel)
    {
        $channels = $channel->lists('title', 'id');

        return view('admin.posts.edit', compact('post', 'channels'));
    }

    public function update(Post $post, UpdatePostRequest $request)
    {
        $data = $request->except('_method', '_token');

        $post = $this->post->update($data, $post->id);

        flash()->success("Post editado com sucesso.");

        return redirect()->route('admin.post.edit', $post->slug);
    }

    public function destroy(Post $post)
    {
        $post->delete();

        flash()->success("Post excluído com sucesso.");

        return redirect()->route('admin.post.index');
    }
}
