<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateChannelRequest;
use App\Http\Requests\UpdateChannelRequest;
use App\Models\Channel;
use App\Repositories\ChannelRepository;

class ChannelController extends Controller
{
    private $channel;

    public function __construct(ChannelRepository $channel)
    {
        $this->channel = $channel;
    }

    public function index()
    {
        $channels = $this->channel->paginate(10);

        return view('admin.channels.index', compact('channels'));
    }

    public function create()
    {
        return view('admin.channels.create');
    }

    public function store(CreateChannelRequest $request)
    {
        $channel = $this->channel->create($request->except('_token'));

        if ($channel) {
            flash()->success('Canal criado com sucesso.');
        } else {
            flash()->error('Ocorreu um erro ao tentar criar um novo canal.');
        }

        return redirect()->route('admin.channel.index');
    }

    public function edit(Channel $channel)
    {
        return view('admin.channels.edit', compact('channel'));
    }

    public function update(Channel $channel, UpdateChannelRequest $request)
    {
        $data = $request->except('_method', '_token');

        $channel = $this->channel->update($data, $channel->id);

        flash()->success('Canal atualizado com sucesso.');

        return redirect()->route('admin.channel.edit', $channel->slug);
    }

    public function destroy(Channel $channel)
    {
        try {
            $channel->delete();
            flash()->success('Canal excluído com sucesso.');
        } catch (\Illuminate\Database\QueryException $e) {
            flash()->error('Não foi possível excluir o canal: existem posts relacionados.');
        }

        return redirect()->route('admin.channel.index');
    }
}
