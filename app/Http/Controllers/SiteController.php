<?php

namespace App\Http\Controllers;

use App\Repositories\PostRepository;
use App\Repositories\CalendarRepository;
use App\Repositories\SaintRepository;
use App\Repositories\SlideRepository;
use App\Repositories\ChannelRepository;
use App\Repositories\GalleryRepository;

class SiteController extends Controller
{
    public function index(
        PostRepository $post,
        SaintRepository $saint,
        SlideRepository $slide,
        ChannelRepository $channel
    )
    {
        $news = $post->getByChannelSlug('noticias', 2);
        $slides = $slide->allPublished();
        $channelNews = $post->getChannelNews(4);
        $channelNewsFirst = array_pull($channelNews, 0);
        $saintOfTheDay = $saint->findOfTheDay();
        $presidentWord = $post->getByChannelSlug('palavra-do-presidente', 1)[0];
        $metropolitanNews = $post->getByChannelSlug('noticias-metropolitanas', 2);

        return view('site.index', compact(
            'news',
            'slides',
            'channelNews',
            'saintOfTheDay',
            'presidentWord',
            'channelNewsFirst',
            'metropolitanNews')
        );
    }

    public function rules()
    {
        return view('site.rules');
    }

    public function calendar(CalendarRepository $calendar, $slug = null)
    {
        $numberCalendars = 7;

        if ($slug) {
            $firstCalendar = \App\Models\Calendar::whereSlug($slug)->firstOrFail();

            $numberCalendars = 6;
        }

        $calendars = $calendar->nextCalendars($numberCalendars);

        if (is_null($slug)) {
            $firstCalendar = array_pull($calendars, 0);
        }

        return view('site.calendar', compact('firstCalendar', 'calendars'));
    }

    public function director()
    {
        return view('site.director');
    }

    public function history()
    {
        return view('site.history');
    }

    public function contact()
    {
        return view('site.contact');
    }

    public function postContact(\App\Http\Requests\ContactRequest $request)
    {
        $data = $request->all();

        app('mailer')->send('emails.contact_admin', $data, function ($mail) use ($data) {
            $mail->to('contato@ssvpcma.org.br')->subject('Site SSVP - '.$data['subject']);
        });

        app('mailer')->send('emails.contact', $data, function ($mail) use ($data) {
            $mail->to($data['email'])->subject('SSVP - '.$data['subject'])->replyTo('contato@ssvpcma.org.br');
        });

        flash()->success('E-mail enviado com sucesso.');

        return redirect()->route('site.contact');
    }

    public function channel(\App\Models\Channel $channel)
    {
        return view('site.channel', compact('channel'));
    }

    public function post(\App\Models\Channel $channel, \App\Models\Post $post)
    {
        return view('site.post', compact('channel', 'post'));
    }

    public function galleries(GalleryRepository $gallery)
    {
        $galleries = $gallery->all();

        return view('site.gallery', compact('galleries'));
    }

    public function gallery(\App\Models\Gallery $gallery)
    {
        return view('site.photos', compact('gallery'));
    }

    public function saints(SaintRepository $saint)
    {
        $saints = $saint->paginate(10);

        return view('site.saints', compact('saints'));
    }

    public function saint(\App\Models\Saint $saint)
    {
        return view('site.saint', compact('saint'));
    }

    public function downloadRules()
    {
        return response()->download(public_path('uploads/rules.pdf'), 'regras-SSVP.pdf');
    }
}
