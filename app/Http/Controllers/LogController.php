<?php

namespace App\Http\Controllers;

use File;
use App\Services\LogViewer;
use Illuminate\Http\Request;

class LogController extends Controller
{
    public function index(Request $request)
    {
        if ($request->input('l')) {
            LogViewer::setFile(base64_decode($request->input('l')));
        }

        if ($request->input('dl')) {
            return response()->download(storage_path() . '/logs/' . base64_decode($request->input('dl')));
        } elseif ($request->has('del')) {
            File::delete(storage_path() . '/logs/' . base64_decode($request->input('del')));

            return redirect($request->url());
        }

        $logs = LogViewer::all();

        return view('admin.log', [
            'logs'         => $logs,
            'files'        => LogViewer::getFiles(true),
            'current_file' => LogViewer::getFileName(),
        ]);
    }
}
