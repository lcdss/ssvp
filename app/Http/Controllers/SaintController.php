<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSaintRequest;
use App\Http\Requests\UpdateSaintRequest;
use App\Models\Saint;
use App\Repositories\SaintRepository;

class SaintController extends Controller
{
    private $saint;

    public function __construct(SaintRepository $saint)
    {
        $this->saint = $saint;
    }

    public function index()
    {
        $saints = $this->saint->paginate(10);

        return view('admin.saints.index', compact('saints'));
    }

    public function create()
    {
        return view('admin.saints.create');
    }

    public function store(CreateSaintRequest $request)
    {
        $data = $request->except('_token');

        $saint = $this->saint->create($data);

        flash()->success('Santo criado com sucesso.');

        return redirect()->route('admin.saint.index');
    }

    public function edit(Saint $saint)
    {
        return view('admin.saints.edit', compact('saint'));
    }

    public function update(Saint $saint, UpdateSaintRequest $request)
    {
        $data = $request->except('_method', '_token');

        $saint = $this->saint->update($data, $saint->id);

        flash()->success("Santo editado com sucesso.");

        return redirect()->route('admin.saint.edit', $saint->slug);
    }

    public function destroy(Saint $saint)
    {
        $saint->delete();

        flash()->success("Santo excluído com sucesso.");

        return redirect()->route('admin.saint.index');
    }
}
