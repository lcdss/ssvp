<?php

namespace App\Http\Controllers;

use Newsletter;
use Illuminate\Http\Request;

class NewsLetterController extends Controller
{
    public function subscribe(Request $request)
    {
        Newsletter::subscribe($request->input('email'));

        return redirect()->back();
    }

    public function unsubscribe()
    {
        Newsletter::unsubscribe($request->input('email'));

        return redirect()->back();
    }
}
