<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends Controller
{
    protected $redirectPath = '/admin';

    protected $loginPath = '/auth/login';

    protected $redirectAfterLogout = '/';

    protected $username = 'email';

    protected $lockoutTime = '600';

    protected $maxLoginAttempts = '10';

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function authenticated($request, $user)
    {
        $user->update(['last_login' => date('Y-m-d H:i:s')]);

        return redirect()->intended($this->redirectPath());
    }
}
