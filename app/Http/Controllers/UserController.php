<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    /**
     * User repository instance.
     * @var \App\Repositories\UserRepository
     */
    private $user;

    /**
     * Inject a user repository dependency.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = $this->user->allWithPaginate(['image']);

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->except('_token', 'password_confirmation');

        $this->user->create($data);

        flash()->success("Usuário criado com sucesso.");

        return redirect()->route('admin.user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  User $user
     * @return Response
     */
    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User $user
     * @return Response
     */
    public function edit(User $user)
    {
        $user->image;
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  User $user
     * @param  Request $request
     * @return Response
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $data = $request->except('_method', '_token', 'password_confirmation');

        $user = $this->user->update($data, $user->id);

        flash()->success("Usuário foi editado com sucesso.");

        return redirect()->route('admin.user.edit', $user->username);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        flash()->success("Usuário excluído com sucesso.");

        return redirect()->route('admin.user.index');
    }

    public function password(User $user, UpdatePasswordRequest $request)
    {
        $user->password = bcrypt($request->input('password'));

        $user->save();

        flash()->success("Senha alterada com sucesso.");

        return redirect()->route('admin.user.edit', $user->username);
    }

    public function getProfile(User $user)
    {
        return view('admin.users.profile', compact('user'));
    }

    public function postProfile(User $user, UpdateProfileRequest $request)
    {
        $data = $request->except('_method', '_token', 'current_password');

        $user = $this->user->update($data, $user->id);

        flash()->success("Perfil salvo com sucesso.");

        return redirect()->route('admin.user.profile.get', $user->username);
    }
}
