<?php

namespace App\Http\Controllers;

use App\Repositories\SlideRepository;
use App\Http\Requests\CreateSlideRequest;
use App\Http\Requests\UpdateSlideRequest;

class SlideController extends Controller
{
    private $slide;

    public function __construct(SlideRepository $slide)
    {
        $this->slide = $slide;
    }

    public function index()
    {
        $slides = $this->slide->all()->sortByDesc('is_published');

        return view('admin.slides.index', compact('slides'));
    }

    public function store(CreateSlideRequest $request)
    {
        $this->slide->create($request->except('_token'));

        flash()->success('Slide criado com sucesso.');

        return redirect()->route('admin.slide.index');
    }

    public function destroy($id)
    {
        $this->slide->delete($id);

        flash()->success('Slide excluído com sucesso.');

        return redirect()->route('admin.slide.index');
    }

    public function update($id, UpdateSlideRequest $request)
    {
        $this->slide->find($id)->update($request->only(['title', 'body']));

        flash()->success('Slide atualizado com sucesso.');

        return redirect()->route('admin.slide.index');
    }

    public function toggleStatus($id)
    {
        $slide = $this->slide->toggleStatus($id);

        if ($slide->is_published)
            flash()->success("Slide ativado com sucesso.");
        else
            flash()->success("Slide desativado com sucesso.");

        return redirect()->route('admin.slide.index');
    }
}
