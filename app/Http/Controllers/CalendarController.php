<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCalendarRequest;
use App\Http\Requests\UpdateCalendarRequest;
use App\Models\Calendar;
use App\Repositories\CalendarRepository;

class CalendarController extends Controller
{
    private $calendar;

    public function __construct(CalendarRepository $calendar)
    {
        $this->calendar = $calendar;
    }

    public function index()
    {
        $calendars = $this->calendar->paginate(10);

        return view('admin.calendars.index', compact('calendars'));
    }

    public function create()
    {
        return view('admin.calendars.create');
    }

    public function store(CreateCalendarRequest $request)
    {
        $data = $request->except('_token');

        $calendar = $this->calendar->create($data);

        flash()->success('Calendaro criado com sucesso.');

        return redirect()->route('admin.calendar.index');
    }

    public function edit(Calendar $calendar)
    {
        return view('admin.calendars.edit', compact('calendar'));
    }

    public function update(Calendar $calendar, UpdateCalendarRequest $request)
    {
        $data = $request->except('_method', '_token');

        $calendar = $this->calendar->update($data, $calendar->id);

        flash()->success("Calendaro editado com sucesso.");

        return redirect()->route('admin.calendar.edit', $calendar->slug);
    }

    public function destroy(Calendar $calendar)
    {
        $calendar->delete();

        flash()->success("Calendaro excluído com sucesso.");

        return redirect()->route('admin.calendar.index');
    }
}
