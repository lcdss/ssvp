<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Repositories\GalleryRepository;
use App\Http\Requests\CreateGalleryRequest;
use App\Http\Requests\UpdateGalleryRequest;

class GalleryController extends Controller
{
    private $gallery;

    public function __construct(GalleryRepository $gallery)
    {
        $this->gallery = $gallery;
    }

    public function index()
    {
        $galleries = $this->gallery->paginate(10);

        return view('admin.galleries.index', compact('galleries'));
    }

    public function create()
    {
        return view('admin.galleries.create');
    }

    public function store(CreateGalleryRequest $request)
    {
        $gallery = $this->gallery->create($request->except('_token'));

        if ($gallery) {
            flash()->success('Galeria criada com sucesso.');
        } else {
            flash()->error('Ocorreu um erro ao tentar criar uma nova galeria.');
        }

        return redirect()->route('admin.gallery.index');
    }

    public function edit(Gallery $gallery)
    {
        return view('admin.galleries.edit', compact('gallery'));
    }

    public function update(Gallery $gallery, UpdateGalleryRequest $request)
    {
        $data = $request->except('_method', '_token');

        $gallery = $this->gallery->update($data, $gallery->id);

        flash()->success('Galeria atualizada com sucesso.');

        return redirect()->route('admin.gallery.edit', $gallery->slug);
    }

    public function destroy(Gallery $gallery)
    {
        $gallery->delete();

        flash()->success('Galeria excluída com sucesso.');

        return redirect()->route('admin.gallery.index');
    }

    public function destroyImage(Gallery $gallery, \App\Models\Image $image)
    {
        $image->delete();

        flash()->success('Foto excluída com sucesso.');

        return redirect()->route('admin.gallery.edit', $gallery->slug);
    }
}
