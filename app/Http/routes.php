<?php

$router->get('/', ['as' => 'site.index', 'uses' => 'SiteController@index']);
$router->get('regras', ['as' => 'site.rules', 'uses' => 'SiteController@rules']);
$router->get('regras/download', ['as' => 'site.rules.download', 'uses' => 'SiteController@downloadRules']);
$router->get('diretoria', ['as' => 'site.director', 'uses' => 'SiteController@director']);
$router->get('historia', ['as' => 'site.history', 'uses' => 'SiteController@history']);
$router->get('contato', ['as' => 'site.contact', 'uses' => 'SiteController@contact']);
$router->post('contato', ['as' => 'site.contact.post', 'uses' => 'SiteController@postContact']);

$router->get('calendario/{slug?}', ['as' => 'site.calendar', 'uses' => 'SiteController@calendar']);

$router->get('galerias', ['as' => 'site.gallery', 'uses' => 'SiteController@galleries']);
$router->get('galerias/{gallery}', ['as' => 'site.gallery.show', 'uses' => 'SiteController@gallery']);

$router->get('newsletter/subscribe', ['as' => 'newsletter.subscribe', 'uses' => 'NewsletterController@subscribe']);
$router->get('newsletter/unsubscribe', ['as' => 'newsletter.unsubscribe', 'uses' => 'NewsletterController@unsubscribe']);

$router->get('canais/{channel}', ['as' => 'site.channel', 'uses' => 'SiteController@channel']);
$router->get('canais/{channel}/{post}', ['as' => 'site.post', 'uses' => 'SiteController@post']);

$router->get('santos', ['as' => 'site.saint', 'uses' => 'SiteController@saints']);
$router->get('santos/{saint}', ['as' => 'site.saint.show', 'uses' => 'SiteController@saint']);

$router->get('auth/login', ['as' => 'auth.login.get', 'uses' => 'Auth\AuthController@getLogin']);
$router->post('auth/login', ['as' => 'auth.login.post', 'uses' => 'Auth\AuthController@postLogin']);

$router->group(['prefix' => 'admin', 'middleware' => 'auth'], function ($router) {
    $router->get('auth/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@getLogout']);

    $router->get('/', ['as' => 'admin.index', 'uses' => 'AdminController@index']);

    $router->put('users/{user}/password', ['as' => 'admin.user.password', 'uses' => 'UserController@password']);
    $router->put('users/{user}/profile', ['as' => 'admin.user.profile.post', 'uses' => 'UserController@postProfile']);
    $router->get('users/{user}/profile', ['as' => 'admin.user.profile.get', 'uses' => 'UserController@getProfile']);

    $router->get('posts', ['as' => 'admin.post.index', 'uses' => 'PostController@index']);
    $router->get('posts/new', ['as' => 'admin.post.create', 'uses' => 'PostController@create']);
    $router->get('posts/{post}/edit', ['as' => 'admin.post.edit', 'uses' => 'PostController@edit']);
    $router->post('posts', ['as' => 'admin.post.store', 'uses' => 'PostController@store']);
    $router->put('posts/{post}/update', ['as' => 'admin.post.update', 'uses' => 'PostController@update']);
    $router->get('posts/{post}/delete', ['as' => 'admin.post.destroy', 'uses' => 'PostController@destroy']);

    $router->get('saints', ['as' => 'admin.saint.index', 'uses' => 'SaintController@index']);
    $router->get('saints/new', ['as' => 'admin.saint.create', 'uses' => 'SaintController@create']);
    $router->get('saints/{saint}/edit', ['as' => 'admin.saint.edit', 'uses' => 'SaintController@edit']);
    $router->post('saints', ['as' => 'admin.saint.store', 'uses' => 'SaintController@store']);
    $router->put('saints/{saint}/update', ['as' => 'admin.saint.update', 'uses' => 'SaintController@update']);
    $router->get('saints/{saint}/delete', ['as' => 'admin.saint.destroy', 'uses' => 'SaintController@destroy']);

    $router->get('calendars', ['as' => 'admin.calendar.index', 'uses' => 'CalendarController@index']);
    $router->get('calendars/new', ['as' => 'admin.calendar.create', 'uses' => 'CalendarController@create']);
    $router->get('calendars/{calendar}/edit', ['as' => 'admin.calendar.edit', 'uses' => 'CalendarController@edit']);
    $router->post('calendars', ['as' => 'admin.calendar.store', 'uses' => 'CalendarController@store']);
    $router->put('calendars/{calendar}/update', ['as' => 'admin.calendar.update', 'uses' => 'CalendarController@update']);
    $router->get('calendars/{calendar}/delete', ['as' => 'admin.calendar.destroy', 'uses' => 'CalendarController@destroy']);

    $router->get('galleries', ['as' => 'admin.gallery.index', 'uses' => 'GalleryController@index']);
    $router->get('galleries/new', ['as' => 'admin.gallery.create', 'uses' => 'GalleryController@create']);
    $router->get('galleries/{gallery}/edit', ['as' => 'admin.gallery.edit', 'uses' => 'GalleryController@edit']);
    $router->post('galleries', ['as' => 'admin.gallery.store', 'uses' => 'GalleryController@store']);
    $router->put('galleries/{gallery}/update', ['as' => 'admin.gallery.update', 'uses' => 'GalleryController@update']);
    $router->get('galleries/{gallery}/delete', ['as' => 'admin.gallery.destroy', 'uses' => 'GalleryController@destroy']);
    $router->get('galleries/{gallery}/{image}/delete', ['as' => 'admin.gallery.image.destroy', 'uses' => 'GalleryController@destroyImage']);

    $router->get('slides', ['as' => 'admin.slide.index', 'uses' => 'SlideController@index']);
    $router->post('slides', ['as' => 'admin.slide.store', 'uses' => 'SlideController@store']);
    $router->get('slides/{id}/delete', ['as' => 'admin.slide.destroy', 'uses' => 'SlideController@destroy']);
    $router->get('slides/{id}/toggle', ['as' => 'admin.slide.toggle', 'uses' => 'SlideController@toggleStatus']);
    $router->put('slides/{id}/update', ['as' => 'admin.slide.update', 'uses' => 'SlideController@update']);

    $router->group(['middleware' => 'role:admin|owner'], function ($router) {
        $router->get('users', ['as' => 'admin.user.index', 'uses' => 'UserController@index']);
        $router->get('users/new', ['as' => 'admin.user.create', 'uses' => 'UserController@create']);
        $router->get('users/{user}', ['as' => 'admin.user.show', 'uses' => 'UserController@show']);
        $router->get('users/{user}/edit', ['as' => 'admin.user.edit', 'uses' => 'UserController@edit']);
        $router->post('users', ['as' => 'admin.user.store', 'uses' => 'UserController@store']);
        $router->put('users/{user}/update', ['as' => 'admin.user.update', 'uses' => 'UserController@update']);
        $router->get('users/{user}/delete', ['as' => 'admin.user.destroy', 'uses' => 'UserController@destroy']);
    });

    $router->group(['middleware' => 'role:admin'], function ($router) {
        $router->get('channels', ['as' => 'admin.channel.index', 'uses' => 'ChannelController@index']);
        $router->get('channels/new', ['as' => 'admin.channel.create', 'uses' => 'ChannelController@create']);
        $router->get('channels/{channel}/edit', ['as' => 'admin.channel.edit', 'uses' => 'ChannelController@edit']);
        $router->post('channels', ['as' => 'admin.channel.store', 'uses' => 'ChannelController@store']);
        $router->put('channels/{channel}/update', ['as' => 'admin.channel.update', 'uses' => 'ChannelController@update']);
        $router->get('channels/{channel}/delete', ['as' => 'admin.channel.destroy', 'uses' => 'ChannelController@destroy']);

        $router->get('logs', ['as' => 'admin.log.index', 'uses' => 'LogController@index']);
    });
});
