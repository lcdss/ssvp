<?php

Breadcrumbs::register('admin.index', function ($breadcrumbs) {
    $breadcrumbs->push('Painel', route('admin.index'), ['icon' => 'fa fa-dashboard']);
});

Breadcrumbs::register('admin.user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Usuários', route('admin.user.index'), ['icon' => 'fa fa-users']);
});

Breadcrumbs::register('admin.user.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.user.index');
    $breadcrumbs->push('Novo Usuário', route('admin.user.create'), ['icon' => 'fa fa-plus']);
});

Breadcrumbs::register('admin.user.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.user.index');
    $breadcrumbs->push('Editar Usuário', route('admin.user.edit'), ['icon' => 'fa fa-edit']);
});

Breadcrumbs::register('admin.slide.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Slides', route('admin.slide.index'), ['icon' => 'fa fa-sliders']);
});

Breadcrumbs::register('admin.channel.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Canais', route('admin.channel.index'), ['icon' => 'fa fa-newspaper-o']);
});

Breadcrumbs::register('admin.channel.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.channel.index');
    $breadcrumbs->push('Novo Canal', route('admin.channel.create'), ['icon' => 'fa fa-plus']);
});

Breadcrumbs::register('admin.channel.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.channel.index');
    $breadcrumbs->push('Editar Canal', route('admin.channel.edit'), ['icon' => 'fa fa-edit']);
});

Breadcrumbs::register('admin.post.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Posts', route('admin.post.index'), ['icon' => 'fa fa-thumb-tack']);
});

Breadcrumbs::register('admin.post.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.post.index');
    $breadcrumbs->push('Novo Post', route('admin.post.create'), ['icon' => 'fa fa-plus']);
});

Breadcrumbs::register('admin.post.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.post.index');
    $breadcrumbs->push('Editar Post', route('admin.post.edit'), ['icon' => 'fa fa-edit']);
});

Breadcrumbs::register('admin.saint.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Saints', route('admin.saint.index'), ['icon' => 'fa fa-thumb-tack']);
});

Breadcrumbs::register('admin.saint.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.saint.index');
    $breadcrumbs->push('Novo Santo', route('admin.saint.create'), ['icon' => 'fa fa-plus']);
});

Breadcrumbs::register('admin.saint.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.saint.index');
    $breadcrumbs->push('Editar Santo', route('admin.saint.edit'), ['icon' => 'fa fa-edit']);
});

Breadcrumbs::register('admin.calendar.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Calendários', route('admin.calendar.index'), ['icon' => 'fa fa-calendar']);
});

Breadcrumbs::register('admin.calendar.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.calendar.index');
    $breadcrumbs->push('Nova Calendário', route('admin.calendar.create'), ['icon' => 'fa fa-plus']);
});

Breadcrumbs::register('admin.calendar.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.calendar.index');
    $breadcrumbs->push('Editar Calendário', route('admin.calendar.edit'), ['icon' => 'fa fa-edit']);
});

Breadcrumbs::register('admin.gallery.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Galerias', route('admin.gallery.index'), ['icon' => 'fa fa-image']);
});

Breadcrumbs::register('admin.gallery.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.gallery.index');
    $breadcrumbs->push('Nova Galeria', route('admin.gallery.create'), ['icon' => 'fa fa-plus']);
});

Breadcrumbs::register('admin.gallery.edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.gallery.index');
    $breadcrumbs->push('Editar Galeria', route('admin.gallery.edit'), ['icon' => 'fa fa-edit']);
});

Breadcrumbs::register('admin.log.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.index');
    $breadcrumbs->push('Logs', route('admin.log.index'), ['icon' => 'fa fa-file-text-o']);
});
