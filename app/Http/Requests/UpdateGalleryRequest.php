<?php

namespace App\Http\Requests;

class UpdateGalleryRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter('gallery')->id;

        $rules =  [
            'title' => 'required|max:100|unique:galleries,title,'.$id,
            'body'  => 'required|max:500',
        ];

        if (count($this->image) && ! is_null($this->image[0])) {
            foreach ($this->image as $key => $image) {
                $rules['image.'.$key] = 'image|max:2000|mimes:png,jpeg,gif';
            }
        }

        return $rules;
    }
}
