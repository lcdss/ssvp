<?php

namespace App\Http\Requests;

class UpdateCalendarRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter('calendar')->id;

        return [
            'title'    => 'required|max:100|unique:calendars,title,'.$id,
            'body'     => 'required|max:2500',
            'start_at' => 'required|date_format:d/m/Y H:i',
            'end_at'   => 'required|date_format:d/m/Y H:i',
            'image'    => 'image|max:2000|mimes:png,jpeg,gif',
        ];
    }
}
