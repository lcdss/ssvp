<?php

namespace App\Http\Requests;

class UpdateChannelRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter('channel')->id;

        return [
            'title' => 'required|max:30|unique:channels,title,'.$id,
        ];
    }
}
