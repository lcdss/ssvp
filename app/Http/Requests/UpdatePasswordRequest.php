<?php

namespace App\Http\Requests;

class UpdatePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $current_password = \Auth::user()->password;

        return \Hash::check($this->input('current_password'), $current_password);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required',
            'password'         => 'required|pwd|confirmed|min:6|max:18',
        ];
    }
}
