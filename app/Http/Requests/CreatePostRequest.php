<?php

namespace App\Http\Requests;

class CreatePostRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'      => 'required|min:5|max:100|unique:posts,title',
            'channel_id' => 'required|exists:channels,id',
            'body'       => 'required|min:250',
            'image'      => 'image|max:2000|mimes:png,jpeg,gif',
        ];
    }
}
