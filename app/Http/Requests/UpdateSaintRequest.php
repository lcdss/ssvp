<?php

namespace App\Http\Requests;

class UpdateSaintRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name'  => 'required|min:5|max:100',
          'body'  => 'required|min:250',
          'date'  => 'required|date_format:d/m/Y',
          'image' => 'image|max:2000|mimes:png,jpeg,gif',
        ];
    }
}
