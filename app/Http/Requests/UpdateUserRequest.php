<?php

namespace App\Http\Requests;

class UpdateUserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter('user')->id;

        return [
            'first_name' => 'required|max:30',
            'last_name'  => 'required|max:30',
            'username'   => 'required|username|max:20|unique:users,username,'.$id,
            'email'      => 'required|email|max:50|unique:users,email,'.$id,
            'image'      => 'image|max:2000|mimes:png,jpeg,gif',
            'password'   => 'required_with:password_confirmation|pwd|min:6|max:16|confirmed',
        ];
    }
}
