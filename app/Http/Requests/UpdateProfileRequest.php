<?php

namespace App\Http\Requests;

class UpdateProfileRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->getParameter('user')->id;

        return [
            'first_name'       => 'required|max:30',
            'last_name'        => 'required|max:30',
            'username'         => 'required|max:20|unique:users,username,'.$id,
            'email'            => 'required|max:50|unique:users,email,'.$id,
            'image'            => 'image|max:2000|mimes:png,jpeg,gif',
            'current_password' => 'required|current_password',
        ];
    }
}
