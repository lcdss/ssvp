<?php

namespace App\Http\Requests;

class CreateUserRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name'  => 'required',
            'username'   => 'required|unique:users,username',
            'email'      => 'required|unique:users,email',
            'password'   => 'required|pwd|min:6|max:16|confirmed',
            'image'      => 'image|max:2000|mimes:png,jpeg,gif',
        ];
    }
}
