<?php

namespace App\Http\Requests;

class CreateSlideRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'body'  => 'max:250',
            'image' => 'required|image|max:2000|mimes:png,jpeg,gif',
        ];
    }
}
