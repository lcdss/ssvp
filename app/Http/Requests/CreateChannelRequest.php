<?php

namespace App\Http\Requests;

class CreateChannelRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:30|unique:channels,title',
        ];
    }
}
