<?php

namespace App\Models;

use App\Presenters\ChannelPresenter;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

class Channel extends Model implements HasPresenter
{
    protected $fillable = ['title', 'slug', 'body'];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getPresenterClass()
    {
        return ChannelPresenter::class;
    }
}
