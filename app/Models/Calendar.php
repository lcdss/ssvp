<?php

namespace App\Models;

use App\Presenters\CalendarPresenter;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

class Calendar extends Model implements HasPresenter
{
    protected $fillable = ['title', 'slug', 'body', 'start_at', 'end_at'];

    protected $with = ['image'];

    protected $dates = ['start_at', 'end_at'];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function getPresenterClass()
    {
        return CalendarPresenter::class;
    }
}
