<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['title', 'slug', 'body'];

    protected $with = ['images'];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function delete()
    {
        foreach ($this->images as $image) {
            $image->delete();
        }

        return parent::delete();
    }

    public function getRandomImage()
    {
        return $this->images[mt_rand(0, count($this->images) - 1)];
    }
}
