<?php

namespace App\Models;

use App\Presenters\SaintPresenter;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

class Saint extends Model implements HasPresenter
{
    protected $fillable = ['name', 'slug', 'body', 'date'];

    protected $with = ['image'];

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function delete()
    {
        $this->image->delete();

        return parent::delete();
    }

    public function getPresenterClass()
    {
        return SaintPresenter::class;
    }
}
