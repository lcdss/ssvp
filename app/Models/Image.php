<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function imageable()
    {
        return $this->morphTo();
    }

    public function getRelativeUrl()
    {
        return config('cms.upload_path') . $this->path . '/' . $this->getFullName();
    }

    public function getFullName()
    {
        return $this->name . '.' . $this->extension;
    }

    public function delete()
    {
        if (file_exists($this->getFullPath()))
            unlink($this->getFullPath());

        if (count(scandir($this->getPath())) == 2)
            rmdir($this->getPath());

        return parent::delete();
    }

    public function getFullPath()
    {
        return $this->getPath() . '/' . $this->getFullName();
    }

    public function getPath()
    {
        return public_path(config('cms.upload_path')) . $this->path;
    }
}
