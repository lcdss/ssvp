<?php

namespace App\Models;

use App\Presenters\PostPresenter;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

class Post extends Model implements HasPresenter
{
    protected $fillable = ['title', 'slug', 'body', 'is_published', 'channel_id', 'user_id'];

    protected $with = ['image'];

    protected $casts = ['is_published' => 'bool'];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function delete()
    {
        $this->image->delete();

        return parent::delete();
    }

    public function getPresenterClass()
    {
        return PostPresenter::class;
    }
}
