<?php

namespace App\Repositories;

class PostRepository extends AbstractRepository
{
    use HasImageTrait;

    public function model()
    {
        return 'App\Models\Post';
    }

    public function getByChannelSlug($channelSlug, $take = null)
    {
        return $this->model->whereHas('channel', function ($q) use ($channelSlug) {
            $q->whereSlug($channelSlug);
        })->whereIsPublished(1)->orderBy('created_at', 'desc')->take($take)->get();
    }

    public function getChannelNews($take = null)
    {
        return $this->model->whereHas('channel', function ($q) {
            $q->where('slug', 'ecafo')
                ->orWhere('slug', 'cca')
                ->orWhere('slug', 'comissao-de-jovens')
                ->orWhere('slug', 'denor')
                ->orWhere('slug', 'decom');
        })->whereIsPublished(1)->orderBy('created_at', 'desc')->take($take)->get();
    }

    public function create(array $data)
    {
        $data['slug'] = str_slug($data['title']);
        $data['title'] = title_case($data['title']);
        $data['user_id'] = \Auth::id();
        $data['is_published'] = isset($data['is_published']) ? true : false;

        if (isset($data['image'])) {
            $image = $this->image->createAndSaveFile(array_pull($data, 'image'), $data['title'], $this->getPath());
        }

        $post = $this->model->create($data);

        if (isset($image)) {
            $post->image()->save($image);
        }

        return $post;
    }

    private function getPath()
    {
        return '/posts/' . date('Y') . '-' . date('m');
    }

    public function update(array $data, $id)
    {
        $data['slug'] = str_slug($data['title']);
        $data['title'] = title_case($data['title']);
        $data['is_published'] = isset($data['is_published']) ? true : false;

        $post = $this->model->find($id);

        if (isset($data['image'])) {
            if ($post->image) {
                $post->image->delete();
            }

            $image = $this->image->createAndSaveFile(array_pull($data, 'image'), $data['title'], $this->getPath());
            $post->image()->save($image);
        }

        $post->update($data);

        return $post;
    }
}
