<?php

namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

abstract class AbstractRepository extends Repository
{
    public function allWith(array $relations, $callback = null, $columns = ['*'])
    {
        $this->applyCriteria();

        return $this->model->with($relations, $callback)->get($columns);
    }

    public function allWithPaginate(array $relations, $callback = null, $paginate = 10)
    {
        $this->applyCriteria();

        return $this->model->with($relations, $callback)->paginate($paginate);
    }
}
