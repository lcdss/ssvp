<?php

namespace App\Repositories;

use Illuminate\Container\Container as App;
use Illuminate\Support\Collection;

trait HasImageTrait
{
    private $image;

    public function __construct(App $app, Collection $collection, ImageRepository $image)
    {
        $this->image = $image;
        parent::__construct($app, $collection);
    }
}
