<?php

namespace App\Repositories;

class SlideRepository extends AbstractRepository
{
    use HasImageTrait;

    public function model()
    {
        return 'App\Models\Slide';
    }

    public function allPublished(array $columns = ['*'])
    {
        return $this->model->whereIsPublished(1)->get($columns);
    }

    public function create(array $data)
    {
        $image = $this->image->createAndSaveFile(array_pull($data, 'image'), $data['title'], $this->getPath());
        $slide = $this->model->create($data);
        $slide->image()->save($image);

        return $slide;
    }

    private function getPath()
    {
        return '/slides';
    }

    public function toggleStatus($id)
    {
        $slide = $this->model->find($id);

        $slide->is_published = ! $slide->is_published;

        $slide->save();

        return $slide;
    }

    public function delete($id)
    {
        $slide = $this->model->find($id);
        $slide->image->delete();

        return $slide->delete();
    }
}
