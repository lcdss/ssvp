<?php

namespace App\Repositories;

class ChannelRepository extends AbstractRepository
{
    public function model()
    {
        return 'App\Models\Channel';
    }

    public function create(array $data)
    {
        $data['slug'] = str_slug($data['title']);
        $data['title'] = title_case($data['title']);

        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        $data['slug'] = str_slug($data['title']);
        $data['title'] = title_case($data['title']);

        $channel = $this->model->find($id);
        $channel->update($data);

        return $channel;
    }
}
