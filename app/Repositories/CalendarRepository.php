<?php

namespace App\Repositories;

class CalendarRepository extends AbstractRepository
{
    use HasImageTrait;

    public function model()
    {
        return 'App\Models\Calendar';
    }

    public function nextCalendar()
    {
        return $this->model->orderBy('start_at')
            ->where('start_at', '>=', \DB::raw('NOW()'))
            ->orWhere('end_at', '<=', \DB::raw('NOW()'))
            ->first();
    }

    public function nextCalendars($number)
    {
        return $this->model->orderBy('start_at')
            ->where('start_at', '>=', \DB::raw('NOW()'))
            ->orWhere('end_at', '<=', \DB::raw('NOW()'))
            ->take($number)->get();
    }

    public function create(array $data)
    {
        $data['slug'] = str_slug($data['title']);
        $data['title'] = title_case($data['title']);
        $data['start_at'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $data['start_at'])->toDateTimeString();
        $data['end_at'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $data['end_at'])->toDateTimeString();

        if (isset($data['image'])) {
            $image = $this->image->createAndSaveFile(array_pull($data, 'image'), $data['title'], $this->getPath());
        }

        $calendar = $this->model->create($data);

        if (isset($image)) {
            $calendar->image()->save($image);
        }

        return $calendar;
    }

    private function getPath()
    {
        return '/calendars/' . date('Y') . '-' . date('m');
    }

    public function update(array $data, $id)
    {
        $data['slug'] = str_slug($data['title']);
        $data['title'] = title_case($data['title']);
        $data['start_at'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $data['start_at'])->toDateTimeString();
        $data['end_at'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $data['end_at'])->toDateTimeString();

        $calendar = $this->model->find($id);

        if (isset($data['image'])) {
            if ($calendar->image) {
                $calendar->image->delete();
            }

            $image = $this->image->createAndSaveFile(array_pull($data, 'image'), $data['title'], $this->getPath());
            $calendar->image()->save($image);
        }

        $calendar->update($data);

        return $calendar;
    }
}
