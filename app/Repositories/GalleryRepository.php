<?php

namespace App\Repositories;

class GalleryRepository extends AbstractRepository
{
    use HasImageTrait;

    public function model()
    {
        return 'App\Models\Gallery';
    }

    public function create(array $data)
    {
        $data['slug'] = str_slug($data['title']);
        $data['title'] = title_case($data['title']);

        $gallery = $this->model->create($data);

        $images = [];

        if (isset($data['image']) && $data['image'][0]) {
            foreach (array_pull($data, 'image') as $image) {
                $images[] = $this->image->createAndSaveFile(
                    $image,
                    $data['title'],
                    $this->getPath($gallery->slug)
                );
            }

            $gallery->images()->saveMany($images);
        }

        return $gallery;
    }

    private function getPath($slug)
    {
        return '/galleries/' . date('Y') . '/' . $slug;
    }

    public function update(array $data, $id)
    {
        $data['slug'] = str_slug($data['title']);
        $data['title'] = title_case($data['title']);

        $gallery = $this->model->find($id);

        if (isset($data['image']) && $data['image'][0]) {
            $images = [];

            foreach (array_pull($data, 'image') as $image) {
                $images[] = $this->image->createAndSaveFile(
                    $image,
                    $data['title'],
                    $this->getPath($gallery->slug)
                );
            }

            $gallery->images()->saveMany($images);
        }

        $gallery->update($data);

        return $gallery;
    }
}
