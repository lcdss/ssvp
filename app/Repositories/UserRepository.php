<?php

namespace App\Repositories;

class UserRepository extends AbstractRepository
{
    use HasImageTrait;

    public function model()
    {
        return 'App\Models\User';
    }

    public function create(array $data)
    {
        $data['password'] = bcrypt($data['password']);

        $user = $this->model->create($data);

        if (isset($data['image'])) {
            $image = $this->image->createAndSaveFile(array_pull($data, 'image'), $data['username'], $this->getPath());
            $user->image()->save($image);
        }

        return $user;
    }

    public function getPath()
    {
        return '/users';
    }

    public function update(array $data, $id)
    {
        $user = $this->model->find($id);

        if (empty($data['password'])) {
            unset($data['password']);
        } else {
            $data['password'] = bcrypt($data['password']);
        }

        if (isset($data['image'])) {
            if (isset($user->image)) {
                $user->image->delete();
            }

            $image = $this->image->createAndSaveFile(
                array_pull($data, 'image'), $data['username'], $this->getPath()
            );

            $user->image()->save($image);
        }

        $user->update($data);

        return $user;
    }
}
