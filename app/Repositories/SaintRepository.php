<?php

namespace App\Repositories;

class SaintRepository extends AbstractRepository
{
    use HasImageTrait;

    public function model()
    {
        return 'App\Models\Saint';
    }

    public function findOfTheDay()
    {
        return $this->model->where(\DB::raw('MONTH(date)'), '=', \DB::raw('MONTH(CURDATE())'))
            ->where(\DB::raw('DAY(date)'), '=', \DB::raw('DAY(CURDATE())'))
            ->orderBy(\DB::raw('RAND()'))
            ->first();
    }

    public function create(array $data)
    {
        $data['slug'] = str_slug($data['name']);
        $data['name'] = title_case($data['name']);
        $data['date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $data['date'])->toDateTimeString();

        $image = $this->image->createAndSaveFile(array_pull($data, 'image'), $data['name'], $this->getPath());
        $saint = $this->model->create($data);
        $saint->image()->save($image);

        return $saint;
    }

    private function getPath()
    {
        return '/saints/' . date('Y') . '-' . date('m');
    }

    public function update(array $data, $id)
    {
        $data['slug'] = str_slug($data['name']);
        $data['name'] = title_case($data['name']);
        $data['date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $data['date'])->toDateTimeString();

        $saint = $this->model->find($id);

        if (isset($data['image'])) {
            if ($saint->image) {
                $saint->image->delete();
            }

            $image = $this->image->createAndSaveFile(array_pull($data, 'image'), $data['name'], $this->getPath());
            $saint->image()->save($image);
        }

        $saint->update($data);

        return $saint;
    }
}
