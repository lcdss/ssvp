<?php

namespace App\Repositories;

use App\Models\Image;
use Image as Intervention;

class ImageRepository extends AbstractRepository
{
    public function model()
    {
        return 'App\Models\Image';
    }

    public function createAndSaveFile($fileUploaded, $fileName, $path = '')
    {
        $image = new Image();
        $image->path = $path;
        $image->name = uniqid(str_slug($fileName) . '-');
        $image->extension = $fileUploaded->getClientOriginalExtension();

        $file = Intervention::make($fileUploaded->getRealPath());

        if (! file_exists($image->getPath()))
            mkdir($image->getPath(), 0777, true);

        $file->save($image->getFullPath());

        $image->mime_type = $file->mime();
        $image->width = $file->width();
        $image->height = $file->height();
        $image->size = $file->filesize();
        $image->alt = $fileName;

        return $image;
    }
}
