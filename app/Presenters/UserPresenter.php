<?php

namespace App\Presenters;

use App\Models\User;
use Carbon\Carbon;

class UserPresenter extends AbstractPresenter
{
    public function __construct(User $resource)
    {
        parent::__construct($resource);
    }

    public function last_login()
    {
        $last_login = $this->wrappedObject->last_login;

        if ($last_login === '0000-00-00 00:00:00')
            return "nunca";

        return Carbon::createFromFormat('Y-m-d H:i:s', $last_login)->diffForHumans();
    }

    public function name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
