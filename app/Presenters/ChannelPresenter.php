<?php

namespace App\Presenters;

use App\Models\Channel;

class ChannelPresenter extends AbstractPresenter
{
    public function __construct(Channel $resource)
    {
        parent::__construct($resource);
    }
}
