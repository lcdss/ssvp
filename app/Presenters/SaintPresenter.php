<?php

namespace App\Presenters;

use Carbon\Carbon;
use App\Models\Saint;

class SaintPresenter extends AbstractPresenter
{
    public function __construct(Saint $resource)
    {
        parent::__construct($resource);
    }

    public function date()
    {
        return $this->getDate()->format('d/m/Y');
    }

    public function day()
    {
        return $this->getDate()->day;
    }

    public function month()
    {
        return static::$months[$this->getDate()->month - 1];
    }

    public function year()
    {
        return $this->getDate()->year;
    }

    private function getDate()
    {
        $date = $this->wrappedObject->date;

        return Carbon::createFromFormat('Y-m-d', $date);
    }
}
