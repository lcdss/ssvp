<?php

namespace App\Presenters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\BasePresenter;

abstract class AbstractPresenter extends BasePresenter
{
    protected static $months = [
        0 => 'Jan',
        1 => 'Fev',
        2 => 'Mar',
        3 => 'Abr',
        4 => 'Mai',
        5 => 'Jun',
        6 => 'Jul',
        7 => 'Ago',
        8 => 'Set',
        9 => 'Out',
        10 => 'Nov',
        11 => 'Dez',
    ];

    public function __construct(Model $resource)
    {
        $this->wrappedObject = $resource;
        Carbon::setLocale('pt_BR');
    }

    public function created_at()
    {
        $created = $this->wrappedObject->created_at;

        return Carbon::createFromFormat('Y-m-d H:i:s', $created)
            ->diffForHumans();
    }

    public function updated_at()
    {
        $updated = $this->wrappedObject->updated_at;

        return Carbon::createFromFormat('Y-m-d H:i:s', $updated)
            ->diffForHumans();
    }
}
