<?php

namespace App\Presenters;

use App\Models\Calendar;
use Carbon\Carbon;

class CalendarPresenter extends AbstractPresenter
{
    public function __construct(Calendar $resource)
    {
        parent::__construct($resource);
    }

    public function start_at()
    {
        $start = $this->wrappedObject->start_at;

        return $start->format('d/m/Y H:i');
    }

    public function end_at()
    {
        $end = $this->wrappedObject->end_at;

        return $end->format('d/m/Y H:i');
    }

    public function day()
    {
        $start = $this->wrappedObject->start_at;

        return $start->day;
    }

    public function month()
    {
        $start = $this->wrappedObject->start_at;

        return static::$months[$start->month - 1];
    }

    public function year()
    {
        $start = $this->wrappedObject->start_at;

        return $start->year;
    }

    public function time()
    {
        $start = $this->wrappedObject->start_at;

        return $start->format('H:i');
    }
}
