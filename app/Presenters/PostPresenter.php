<?php

namespace App\Presenters;

use App\Models\Post;

class PostPresenter extends AbstractPresenter
{
    public function __construct(Post $resource)
    {
        parent::__construct($resource);
    }
}
