<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $router->bind('user', function ($username) {
            return \App\Models\User::whereUsername($username)->first();
        });

        $router->bind('channel', function ($slug) {
            return \App\Models\Channel::whereSlug($slug)->first();
        });

        $router->bind('post', function ($slug) {
            return \App\Models\Post::whereSlug($slug)->first();
        });

        $router->bind('saint', function ($slug) {
            return \App\Models\Saint::whereSlug($slug)->first();
        });

        $router->bind('calendar', function ($slug) {
            return \App\Models\Calendar::whereSlug($slug)->first();
        });

        $router->bind('gallery', function ($slug) {
            return \App\Models\Gallery::whereSlug($slug)->first();
        });

        $router->bind('image', function ($id) {
            return \App\Models\Image::find($id);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
