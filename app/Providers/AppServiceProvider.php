<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Validator::resolver(function($translator, $data, $rules, $messages) {
            return new \App\Services\Validation($translator, $data, $rules, $messages);
        });

        $this->app->singleton(\Faker\Generator::class, function () {
            return \Faker\Factory::create('pt_BR');
        });

        \Carbon\Carbon::setLocale('pt_BR');

        view()->composer('site.partials.header', function ($view) {
            $channels = \App\Models\Channel::where('slug', '!=', 'noticias')
                ->where('slug', '!=', 'noticias-metropolitanas')
                ->where('slug', '!=', 'palavra-do-presidente')
                ->get();

            $news = \App\Models\Channel::where('slug', 'noticias')
                ->orWhere('slug', 'noticias-metropolitanas')
                ->orWhere('slug', 'palavra-do-presidente')
                ->get();

            $view->with(['channels' => $channels, 'news' => $news]);
        });

        view()->composer('site.partials.calendar', function ($view) {
            $nextCalendars = \App\Models\Calendar::orderBy('start_at')
                ->where('start_at', '>=', \DB::raw('NOW()'))
                ->orWhere('end_at', '<=', \DB::raw('NOW()'))
                ->take(5)->get();

            $view->with('nextCalendars', $nextCalendars);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      //
    }
}
