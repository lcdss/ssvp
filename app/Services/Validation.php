<?php

namespace App\Services;

use Illuminate\Validation\Validator;

class Validation extends Validator
{
    public function validateCurrentPassword($attribute, $value, $parameters)
    {
        return \Hash::check($value, \Auth::user()->password);
    }

    public function validateName($attribute, $value, $parameters)
    {
        if (! is_string($value) && ! is_numeric($value)) {
            return false;
        }

        return preg_match('/^[\pL\pM\pN\s]+$/u', $value);
    }

    public function validateUsername($attribute, $value, $parameters)
    {
        return preg_match("/^[a-z0-9_-]{4,16}$/", $value);
    }

    public function validateSlug($attribute, $value, $parameters)
    {
        return preg_match("/^[a-z0-9-]+$/", $value);
    }

    public function validatePwd($attribute, $value, $parameters)
    {
        return preg_match("/^(?=.*\d)(?=.*[a-zA-Z])(?!.*\s).{8,16}$/", $value);
    }

    public function validateAlphaSpace($attribute, $value, $parameters)
    {
        return preg_match("/^(a-zA-Z\s){0,}$/", $value);
    }
}
