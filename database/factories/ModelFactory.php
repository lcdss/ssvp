<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function ($faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'username' => $faker->userName,
        'email' => $faker->email,
        'password' => bcrypt('123456'),
    ];
});

$factory->define(App\Models\Channel::class, function($faker) {
    return [
        'title' => $faker->sentence,
        'slug' => $faker->slug,
    ];
});

$factory->define(App\Models\Post::class, function($faker) {
    return [
        'title' => $faker->sentence,
        'slug' => $faker->slug,
        'body' => $faker->text(2000),
        'is_published' => $faker->boolean(),
        'channel_id' => $faker->numberBetween(1, 8),
        'user_id' => $faker->numberBetween(1, 5),
    ];
});

$factory->define(App\Models\Saint::class, function($faker) {
    return [
        'name' => $faker->firstName.' '.$faker->lastName,
        'slug' => $faker->slug,
        'body' => $faker->text(1000),
        'date' => $faker->date(),
    ];
});

$factory->define(App\Models\Gallery::class, function($faker) {
    return [
        'title' => $faker->sentence,
        'slug' => $faker->slug,
        'body' => $faker->text(500),
    ];
});

$factory->define(App\Models\Calendar::class, function($faker) {
    return [
        'title' => $faker->sentence,
        'slug' => $faker->slug,
        'body' => $faker->text(1000),
        'start_at' => $faker->dateTimeBetween('1 month', '6 month'),
        'end_at' => $faker->dateTimeBetween('6 month', '1 year'),
    ];
});

$factory->define(App\Models\Slide::class, function ($faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->paragraph,
    ];
});

$factory->define(App\Models\Image::class, function ($faker) {
    $file = Image::make($faker->image(null, 960, 600));
    return [
        'file' => $file,
        'name' => uniqid(str_slug($faker->word) . '-'),
        'extension' => 'jpg',
        'mime_type' => $file->mime(),
        'width' => $file->width(),
        'height' => $file->height(),
        'size' => $file->filesize(),
        'alt' => $faker->sentence,
    ];
});

$factory->defineAs(App\Models\Image::class, 'user', function ($faker) use ($factory) {
    $image = $factory->raw(App\Models\Image::class);
    $image['path'] = '/users';
    return $image;
});

$factory->defineAs(App\Models\Image::class, 'slide', function ($faker) use ($factory) {
    $image = $factory->raw(App\Models\Image::class);
    $image['path'] = '/slides';
    return $image;
});

$factory->defineAs(App\Models\Image::class, 'post', function ($faker) use ($factory) {
    $image = $factory->raw(App\Models\Image::class);
    $path = public_path(config('cms.upload_path').'/posts/'.date('Y').'-'.date('m'));
    if (! file_exists($path))
        mkdir($path, 0777, true);
    $image['path'] = '/posts/'.date('Y').'-'.date('m');
    return $image;
});

$factory->defineAs(App\Models\Image::class, 'saint', function ($faker) use ($factory) {
    $image = $factory->raw(App\Models\Image::class);
    $path = public_path(config('cms.upload_path').'/saints/'.date('Y').'-'.date('m'));
    if (! file_exists($path))
        mkdir($path, 0777, true);
    $image['path'] = '/saints/'.date('Y').'-'.date('m');
    return $image;
});

$factory->defineAs(App\Models\Image::class, 'calendar', function ($faker) use ($factory) {
    $image = $factory->raw(App\Models\Image::class);
    $path = public_path(config('cms.upload_path').'/calendars/'.date('Y').'-'.date('m'));
    if (! file_exists($path))
        mkdir($path, 0777, true);
    $image['path'] = '/calendars/'.date('Y').'-'.date('m');
    return $image;
});

$factory->defineAs(App\Models\Image::class, 'gallery', function ($faker) use ($factory) {
    $image = $factory->raw(App\Models\Image::class);
    $path = public_path(config('cms.upload_path').'/galleries/'.date('Y'));
    $image['path'] = '/galleries/'.date('Y').'/';
    return $image;
});
