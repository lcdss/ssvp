<?php

use Illuminate\Database\Seeder;

class SaintTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name'         => 'saint.view',
                'display_name' => 'Vizualizar Santo',
            ],
            [
                'name'         => 'saint.create',
                'display_name' => 'Criar Santo',
            ],
            [
                'name'         => 'saint.edit',
                'display_name' => 'Atualizar Santo',
            ],
            [
                'name'         => 'saint.delete',
                'display_name' => 'Excluir Santo',
            ],
        ]);

        factory(App\Models\Saint::class, 5)->create()->each(function($saint) {
            $image = factory(App\Models\Image::class, 'saint')->make();
            $file = $image->file;
            unset($image->file);
            $file->save($image->getFullPath());
            $saint->image()->save($image);
        });
    }
}
