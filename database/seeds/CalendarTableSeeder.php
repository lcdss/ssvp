<?php

use Illuminate\Database\Seeder;

class CalendarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name'         => 'calendar.view',
                'display_name' => 'Vizualizar Calendário',
            ],
            [
                'name'         => 'calendar.create',
                'display_name' => 'Criar Calendário',
            ],
            [
                'name'         => 'calendar.update',
                'display_name' => 'Atualizar Calendário',
            ],
            [
                'name'         => 'calendar.delete',
                'display_name' => 'Excluir Calendário',
            ],
        ]);

        factory(App\Models\Calendar::class, 6)->create()->each(function($calendar) {
            $image = factory(App\Models\Image::class, 'calendar')->make();
            $file = $image->file;
            unset($image->file);
            $file->save($image->getFullPath());
            $calendar->image()->save($image);
        });
    }
}
