<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(ChannelTableSeeder::class);
        $this->call(PostTableSeeder::class);
        $this->call(SaintTableSeeder::class);
        $this->call(SlideTableSeeder::class);
        $this->call(CalendarTableSeeder::class);
        $this->call(GalleryTableSeeder::class);
        $this->call(RoleTableSeeder::class);

        Model::reguard();
    }
}
