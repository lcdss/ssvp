<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name'         => 'user.view',
                'display_name' => 'Vizualizar Usuário',
            ],
            [
                'name'         => 'user.create',
                'display_name' => 'Criar Usuário',
            ],
            [
                'name'         => 'user.update',
                'display_name' => 'Atualizar Usuário',
            ],
            [
                'name'         => 'user.delete',
                'display_name' => 'Excluir Usuário',
            ],
        ]);

        DB::table('users')->insert([
            [
                'id'         => 1,
                'first_name' => 'Lucas',
                'last_name'  => 'Silva',
                'username'   => 'lcdss',
                'email'      => 'lcdss@live.com',
                'password'   => bcrypt('123456'),
                'created_at' => '2015-09-15 00:00:00',
                'updated_at' => '2015-10-08 00:00:00',
            ],
            [
                'id'         => 2,
                'first_name' => 'Owner',
                'last_name'  => 'Owner',
                'username'   => 'owner',
                'email'      => 'owner@ssvpcma.org.br',
                'password'   => bcrypt('123456'),
                'created_at' => '2015-09-25 00:00:00',
                'updated_at' => '2015-09-26 00:00:00',
            ],
            [
                'id'         => 3,
                'first_name' => 'Tiago',
                'last_name'  => 'Souza',
                'username'   => 'tiago',
                'email'      => 'tiago_0310@hotmail.com',
                'password'   => bcrypt('123456'),
                'created_at' => '2015-10-25 00:00:00',
                'updated_at' => '2015-11-07 00:00:00',
            ],
        ]);

        factory(App\Models\User::class, 2)->create()->each(function($user) {
            $image = factory(App\Models\Image::class, 'user')->make();
            $file = $image->file;
            unset($image->file);
            $file->save($image->getFullPath());
            $user->image()->save($image);
        });
    }
}
