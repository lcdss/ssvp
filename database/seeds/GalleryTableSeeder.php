<?php

use Illuminate\Database\Seeder;

class GalleryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name'         => 'gallery.view',
                'display_name' => 'Vizualizar Galeria',
            ],
            [
                'name'         => 'gallery.create',
                'display_name' => 'Criar Galeria',
            ],
            [
                'name'         => 'gallery.edit',
                'display_name' => 'Atualizar Galeria',
            ],
            [
                'name'         => 'gallery.delete',
                'display_name' => 'Excluir Galeria',
            ],
        ]);

        factory(App\Models\Gallery::class, 3)->create()->each(function($gallery) {
            $objects = factory(App\Models\Image::class, 'gallery', 3)->make();

            $images = [];

            foreach($objects as $image) {
                $file = $image->file;
                unset($image->file);
                $image->path .= $gallery->slug;
                if (! file_exists($image->getPath()))
                    mkdir($image->getPath(), 0777, true);
                $file->save($image->getFullPath());
                $images[] = $image;
            }

            $gallery->images()->saveMany($images);
        });
    }
}
