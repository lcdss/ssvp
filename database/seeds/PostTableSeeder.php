<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name'         => 'post.view',
                'display_name' => 'Vizualizar Post',
            ],
            [
                'name'         => 'post.create',
                'display_name' => 'Criar Post',
            ],
            [
                'name'         => 'post.update',
                'display_name' => 'Atualizar Post',
            ],
            [
                'name'         => 'post.delete',
                'display_name' => 'Excluir Post',
            ],
        ]);

        factory(App\Models\Post::class, 20)->create()->each(function($post) {
            $image = factory(App\Models\Image::class, 'post')->make();
            $file = $image->file;
            unset($image->file);
            $file->save($image->getFullPath());
            $post->image()->save($image);
        });
    }
}
