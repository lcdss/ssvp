<?php

use Illuminate\Database\Seeder;

class ChannelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name'         => 'channel.view',
                'display_name' => 'Vizualizar Canal',
            ],
            [
                'name'         => 'channel.create',
                'display_name' => 'Criar Canal',
            ],
            [
                'name'         => 'channel.update',
                'display_name' => 'Atualizar Canal',
            ],
            [
                'name'         => 'channel.delete',
                'display_name' => 'Excluir Canal',
            ],
        ]);

        DB::table('channels')->insert([
            [
                'id' => 1,
                'title' => 'Notícias',
                'slug' => 'noticias',
            ],
            [
                'id' => 2,
                'title' => 'Palavra Do Presidente',
                'slug' => 'palavra-do-presidente',
            ],
            [
                'id' => 3,
                'title' => 'Comissão De Jovens',
                'slug' => 'comissao-de-jovens',
            ],
            [
                'id' => 4,
                'title' => 'Ecafo',
                'slug' => 'ecafo',
            ],
            [
                'id' => 5,
                'title' => 'CCA',
                'slug' => 'cca',
            ],
            [
                'id' => 6,
                'title' => 'Denor',
                'slug' => 'denor',
            ],
            [
                'id' => 7,
                'title' => 'Decom',
                'slug' => 'decom',
            ],
            [
                'id' => 8,
                'title' => 'Notícias Metropolitanas',
                'slug' => 'noticias-metropolitanas',
            ],
        ]);
    }
}
