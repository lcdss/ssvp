<?php

use Illuminate\Database\Seeder;

class SlideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name'         => 'slide.view',
                'display_name' => 'Vizualizar Slide',
            ],
            [
                'name'         => 'slide.create',
                'display_name' => 'Criar Slide',
            ],
            [
                'name'         => 'slide.update',
                'display_name' => 'Atualizar Slide',
            ],
            [
                'name'         => 'slide.delete',
                'display_name' => 'Excluir Slide',
            ],
        ]);

        factory(App\Models\Slide::class, 4)->create()->each(function($slide) {
            $image = factory(App\Models\Image::class, 'slide')->make();
            $file = $image->file;
            unset($image->file);
            $file->save($image->getFullPath());
            $slide->image()->save($image);
        });
    }
}
