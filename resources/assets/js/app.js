$(function () {
  var editor = new MediumEditor('#body:not([class])', {
    buttonLabels: 'fontawesome',
    placeholder: false
  });

  $('#username').mask('U');

  $('#start_at,#end_at').datetimepicker({
    stepping: 5
  });

  $('#date').datetimepicker({
    maxDate: moment(),
    format: 'L'
  });

  $('[data-toggle=popover]').popover({
    html: true,
    trigger: 'manual'
  });

  $('[data-toggle=popover]').on('click', function () {
    if (typeof $(this).attr('aria-describedby') === "undefined")
      $(this).popover('show');
    else
      $(this).popover('hide');
  });

  $('[data-toggle=popover]').on('show.bs.popover', function () {
    $('.popover.fade.top.in').not($(this)).popover('hide');
  });

  $('.fancybox').fancybox({
    openEffect  : 'elastic',
    closeEffect : 'none'
  });
});
