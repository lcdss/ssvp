$(function () {
    var clickEvent = false;

    $('#carousel').carousel({
      interval: 5000
    }).on('click', '.list-group li', function () {
      clickEvent = true;
      $('.list-group li').removeClass('active');
      $(this).addClass('active');
    }).on('slid.bs.carousel', function (e) {
      if (! clickEvent) {
        var count = $('.list-group').children().length - 1;
        var current = $('.list-group li.active');

        current.removeClass('active').next().addClass('active');

        var id = parseInt(current.data('slide-to'));

        if (count === id) {
          $('.list-group li').first().addClass('active');
        }
      }

      clickEvent = false;
    });
});

function ScaleSlider() {
  var boxheight = $('#carousel .carousel-inner').innerHeight();
  var itemlength = $('#carousel .item').length;
  var triggerheight = Math.round(boxheight / itemlength + 1);
  $('#carousel .list-group-item').outerHeight(triggerheight);
}

setTimeout(function () {
  ScaleSlider();
}, 1500);

$(window).bind("load", ScaleSlider);
$(window).bind("resize", ScaleSlider);
$(window).bind("orientationchange", ScaleSlider);
$(window).bind("slid.bs.carousel", ScaleSlider);
