$.extend($.fn.datetimepicker.defaults, {
  locale: 'pt-br',
  showClose: false,
  showClear: false,
  allowInputToggle: true,
  timeZone: 'America/Sao_Paulo',
  icons: {
    time: "fa fa-clock-o",
    date: "fa fa-calendar",
    up: "fa fa-arrow-up",
    down: "fa fa-arrow-down",
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-crosshairs',
    clear: 'fa fa-trash',
    close: 'fa fa-remove'
  },
  tooltips: {
    today: 'Hoje',
    clear: 'Limpar',
    close: 'Fechar',
    selectMonth: 'Selecionar Mês',
    prevMonth: 'Mês Anterior',
    nextMonth: 'Próximo mês',
    selectYear: 'Selecionar Ano',
    prevYear: 'Ano Anterior',
    nextYear: 'Próximo Ano',
    selectDecade: 'Selecionar Década',
    prevDecade: 'Década Anterior',
    nextDecade: 'Próxima Década',
    prevCentury: 'Século Anterior',
    nextCentury: 'Próximo Século',
    selectTime: 'Selecionar Horário',
    incrementHour: 'Aumentar Hora',
    decrementHour: 'Diminuir Hora',
    incrementMinute: 'Aumentar Minuto',
    decrementMinute: 'Diminuir Minuto',
    pickHour: 'Escolher Hora',
    pickMinute: 'Escolher Minuto'
  }
});

$.extend(true, $.fn.dataTable.defaults, {
  language: {
    search: 'Pesquisa ',
    show: 'Mostrar',
    lengthMenu: 'Mostrar _MENU_ registros',
    info: 'Mostrando _START_ à _END_ de _TOTAL_ entradas',
    emptyTable: 'Não há dados disponíveis na tabela',
    infoEmpty: 'Não há dados para mostrar',
    zeroRecords: 'Nenhum registro para mostrar',
    paginate: {
      first:    'Primeiro',
      previous: 'Anterior',
      next:     'Próximo',
      last:     'Último'
    },
    aria: {
      paginate: {
        first:    'Primeiro',
        previous: 'Anterior',
        next:     'Próximo',
        last:     'Último'
      }
    }
  }
});

$.jMaskGlobals = {
  maskElements: 'input',
  dataMask: false,
  watchInterval: 300,
  watchInputs: true,
  watchDataMask: false,
  translation: {
    'U': {pattern: /[a-z0-9]/, recursive: true}
  }
};
