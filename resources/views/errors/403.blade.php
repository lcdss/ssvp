@extends('admin.layout')

@section('title')
Erro 403
@stop

@section('content')
<div class="title">Você não tem permissão para realizar essa ação.</div>
@stop
