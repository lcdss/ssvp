@if ($breadcrumbs)
  <ol class="breadcrumb">
    @foreach ($breadcrumbs as $breadcrumb)
      @if ($breadcrumb->url && !$breadcrumb->last)
        @if(isset($breadcrumb->icon) && $breadcrumb->icon)
          <li>
            <a href="{!! $breadcrumb->url !!}"><i class="{!! $breadcrumb->icon !!}"></i> {!! $breadcrumb->title !!}</a>
          </li>
        @else
          <li><a href="{!! $breadcrumb->url !!}">{!! $breadcrumb->title !!}}</a></li>
        @endif
      @else
        @if(isset($breadcrumb->icon) && $breadcrumb->icon)
          <li class="active"><i class="{!! $breadcrumb->icon !!}"></i> {!! $breadcrumb->title !!}</li>
        @else
          <li class="active">{{{ $breadcrumb->title }}}</li>
        @endif
      @endif
    @endforeach
  </ol>
@endif
