@if (Session::has('flash_notification.message'))
  @if (Session::has('flash_notification.overlay'))
    @include('flash::modal', ['modalClass' => 'flash-modal', 'title' => Session::get('flash_notification.title'), 'body' => Session::get('flash_notification.message')])
  @else
    <div class="alert alert-{{ Session::get('flash_notification.level') }}" data-alert-id="{{ $alertId = uniqid('alert-') }}">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

      {{ Session::get('flash_notification.message') }}
    </div>
    <script>
      window.setTimeout(function () {
        $("[data-alert-id={{ $alertId }}]").fadeTo(750, 0).slideUp(750, function () {
          $(this).remove();
        });
      }, 10000);
    </script>
  @endif
@endif
