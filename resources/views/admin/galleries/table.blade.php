<table class="table table-hover">
  <tr>
    <th>Título</th>
    <th>Descrição</th>
    <th>Opções</th>
  </tr>
  @foreach($galleries as $gallery)
    <tr>
      <td>{!! $gallery->title !!}</td>
      <td>{!! $gallery->body !!}</td>
      <td>
        <a type="button" href="{!! route('admin.gallery.edit', [$gallery->slug]) !!}" class="btn btn-info btn-xs" data-toggle="tooltip" data-container="body" title="Editar"><i class="fa fa-edit"></i></a>
        <a type="button" href="{!! route('admin.gallery.destroy', [$gallery->slug]) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-container="body" title="Excluir"><i class="fa fa-remove"></i></a>
      </td>
    </tr>
  @endforeach
</table>
