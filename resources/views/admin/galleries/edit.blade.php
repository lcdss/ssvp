@extends('admin.layout')

@section('title')
Editar Galeria
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Editar Galeria</div>
  </div>
  <div class="box-body">
    {!! Form::open(['route' => ['admin.gallery.update', $gallery->slug], 'method' => 'put', 'files' => true]) !!}
      @include('admin.galleries.form', ['btn' => 'Salvar'])
    {!! Form::close() !!}
  </div>
  <div class="box-footer">
    <div class="gallery-items photos">
    @foreach($gallery->images as $image)
      <div class="col-xs-6 col-md-3 col-lg-2 popover-item photo-item">
        <a title="Opções"
          data-toggle="popover"
          data-placement="top"
          data-content="<a href='{!! route("admin.gallery.image.destroy", [$gallery->slug, $image->id]) !!}' class='center-block'><i class='fa fa-2x fa-close text-danger'></i></a>">
          <img src="{!! $image->getRelativeUrl() !!}" class="img-responsive" alt="{!! $image->alt !!}">
        </a>
      </div>
    @endforeach
    </div>
  </div>
</div>
@stop
