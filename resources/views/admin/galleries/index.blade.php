@extends('admin.layout')

@section('title')
Galerias
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header">
    <div class="box-title">
      Galerias <span class="label label-warning">{!! $galleries->total() !!}</span>
    </div>
    <div class="box-tools">
      <a href="{!! route('admin.gallery.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Novo</a>
    </div>
  </div>
  <div class="box-body table-responsive no-padding">
    @include('admin.galleries.table')
  </div>
  <div class="box-footer">
    {!! $galleries->render() !!}
  </div>
</div>
@stop
