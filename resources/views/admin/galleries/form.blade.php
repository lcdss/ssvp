<div class="col-xs-12 col-sm-6 form-group {!! ($errors->has('title')) ? 'has-error' : '' !!}{!! (!$errors->has('title')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label" for="title">Nome</label>
  <input type="text" id="title" name="title" value="{!! isset($gallery->title) ? $gallery->title : old('title') !!}" class="form-control" maxlength="30" required>
</div>
<div class="col-xs-12 form-group {!! ($errors->has('body')) ? 'has-error' : '' !!}{!! (!$errors->has('body')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label" for="body">Descrição</label>
  <textarea type="text" id="body" name="body" class="form-control" maxlength="250" rows="3" required>{!! isset($gallery->body) ? $gallery->body : old('body') !!}</textarea>
</div>
<div class="col-xs-12 col-sm-6 form-group {!! ($errors->has('image')) ? 'has-error' : '' !!}{!! (!$errors->has('image')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label" for="image">Fotos</label>
  <input type="file" id="image" name="image[]" accept="image/png,image/jpeg,image/gif" multiple>
</div>
<div class="col-xs-12 form-group">
  <button type="submit" class="btn btn-primary">{!! $btn !!}</button>
</div>
