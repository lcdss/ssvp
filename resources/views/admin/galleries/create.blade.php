@extends('admin.layout')

@section('title')
Nova Galeria
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Nova Galeria</div>
  </div>
  <div class="box-body">
    {!! Form::open(['route' => ['admin.gallery.store'], 'files' => true]) !!}
      @include('admin.galleries.form', ['btn' => 'Criar'])
    {!! Form::close() !!}
  </div>
</div>
@stop
