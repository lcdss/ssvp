@extends('admin.layout')

@section('title')
Editar Santo
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Editar Santo</div>
  </div>
  {!! Form::open(['route' => ['admin.saint.update', $saint->slug], 'files' => true, 'method' => 'put']) !!}
    @include('admin.saints.form', ['btn' => 'Salvar'])
  {!! Form::close() !!}
  </form>
</div>
@stop
