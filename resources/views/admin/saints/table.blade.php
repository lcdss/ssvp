<table class="table table-hover">
  <tr>
    <th>Foto</th>
    <th>Nome</th>
    <th>Data</th>
    <th>Opções</th>
  </tr>
  @foreach($saints as $saint)
    <tr>
      <td>
        <img src="{!! $saint->image ? $saint->image->getRelativeUrl() : 'http://placehold.it/50x50' !!}" width="50" height="50">
      </td>
      <td>{!! $saint->name !!}</td>
      <td>{!! $saint->date !!}</td>
      <td>
        <a type="button" href="{!! route('admin.saint.edit', [$saint->slug]) !!}" class="btn btn-info btn-xs" data-toggle="tooltip" data-container="body" title="Editar"><i class="fa fa-edit"></i></a>
        <a type="button" href="{!! route('admin.saint.destroy', [$saint->slug]) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-container="body" title="Excluir"><i class="fa fa-remove"></i></a>
      </td>
    </tr>
  @endforeach
</table>
