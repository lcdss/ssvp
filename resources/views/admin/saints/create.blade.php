@extends('admin.layout')

@section('title')
Novo Santo
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Novo Santo</div>
  </div>
  {!! Form::open(['route' => ['admin.saint.store'], 'files' => true]) !!}
    @include('admin.saints.form', ['btn' => 'Criar'])
  {!! Form::close() !!}
  </form>
</div>
@stop
