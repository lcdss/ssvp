@extends('admin.layout')

@section('title')
Santos
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header">
    <div class="box-title">
      Santos <span class="label label-warning">{!! $saints->total() !!}</span>
    </div>
    <div class="box-tools">
      <a href="{!! route('admin.saint.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Novo</a>
    </div>
  </div>
  <div class="box-body table-responsive no-padding">
    @include('admin.saints.table')
  </div>
  <div class="box-footer clearfix">
    {!! $saints->render() !!}
  </div>
</div>
@stop
