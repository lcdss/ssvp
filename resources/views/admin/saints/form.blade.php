<div class="box-body">
  <fieldset class="col-xs-12 col-sm-6 form-group {!! ($errors->has('name')) ? 'has-error' : '' !!}{!! (!$errors->has('name')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="name">Título</label>
    <input type="text" id="name" name="name" value="{!! isset($saint->name) ? $saint->name : old('name') !!}" class="form-control" required>
  </fieldset>
  <fieldset class="col-xs-12 col-sm-6 form-group {!! ($errors->has('date')) ? 'has-error' : '' !!}{!! (!$errors->has('date')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="date">Data</label>
    <input type="text" id="date" name="date" value="{!! isset($saint->date) ? $saint->date : old('date') !!}" class="form-control">
  </fieldset>
  <fieldset class="col-xs-12 form-group {!! ($errors->has('body')) ? 'has-error' : '' !!}{!! (!$errors->has('body')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="body">Conteúdo</label>
    <textarea type="text" id="body" name="body">
      @if (isset($saint->body))
        {{ $saint->body }}
      @elseif (old('body') !== null)
        {{ old('body') }}
      @else
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur praesentium dolorem dicta, non est
          facilis reiciendis perferendis architecto, nam voluptate, sequi repellendus esse voluptatibus provident
          aliquam adipisci. Voluptates, ratione, eveniet.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur praesentium dolorem dicta, non est
          facilis reiciendis perferendis architecto, nam voluptate, sequi repellendus esse voluptatibus provident
          aliquam adipisci. Voluptates, ratione, eveniet.</p>
      @endif
    </textarea>
  </fieldset>
  <fieldset class="col-xs-12 col-sm-6 form-group {!! ($errors->has('image')) ? 'has-error' : '' !!}{!! (!$errors->has('image')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="image">Foto</label>
    <input type="file" id="image" name="image" accept="image/png,image/jpeg,image/gif">

    <p class="help-block">A imagem deve estar nos formatos PNG, JPG ou GIF.</p>
    @if (isset($saint->image))
      <img src="{!! $saint->image->getRelativeUrl() !!}" alt="{!! $saint->image->alt !!}" width="250" height="250">
    @endif
  </fieldset>
</div>
<div class="box-footer">
  <button type="submit" class="btn btn-primary">{!! $btn !!}</button>
</div>
