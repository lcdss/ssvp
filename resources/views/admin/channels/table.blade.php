<table class="table table-hover">
  <tr>
    <th>Título</th>
    <th>Opções</th>
  </tr>
  @foreach($channels as $channel)
    <tr>
      <td>{!! $channel->title !!}</td>
      <td>
        <a type="button" href="{!! route('admin.channel.edit', [$channel->slug]) !!}" class="btn btn-info btn-xs" data-toggle="tooltip" data-container="body" title="Editar"><i class="fa fa-edit"></i></a>
        <a type="button" href="{!! route('admin.channel.destroy', [$channel->slug]) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-container="body" title="Excluir"><i class="fa fa-remove"></i></a>
      </td>
    </tr>
  @endforeach
</table>
