@extends('admin.layout')

@section('title')
Novo Canal
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Novo Canal</div>
  </div>
  {!! Form::open(['route' => ['admin.channel.store']]) !!}
    @include('admin.channels.form', ['btn' => 'Criar'])
  {!! Form::close() !!}
  </form>
</div>
@stop
