@extends('admin.layout')

@section('title')
Canais
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header">
    <div class="box-title">
      Canais <span class="label label-warning">{!! $channels->total() !!}</span>
    </div>
    <div class="box-tools">
      <a href="{!! route('admin.channel.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Novo</a>
    </div>
  </div>
  <div class="box-body table-responsive no-padding">
    @include('admin.channels.table')
  </div>
  <div class="box-footer clearfix">
    {!! $channels->render() !!}
  </div>
</div>
@stop
