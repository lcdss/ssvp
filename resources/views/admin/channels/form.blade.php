<div class="box-body">
  <fieldset class="col-xs-12 col-sm-6 form-group {!! ($errors->has('title')) ? 'has-error' : '' !!}{!! (!$errors->has('title')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="title">Nome</label>
    <input type="text" id="title" name="title" value="{!! isset($channel->title) ? $channel->title : old('title') !!}" class="form-control" maxlength="30" required>
  </fieldset>
</div>
<div class="box-footer">
  <button type="submit" class="btn btn-primary">{!! $btn !!}</button>
</div>
