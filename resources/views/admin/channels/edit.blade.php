@extends('admin.layout')

@section('title')
Editar Canal
@stop

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <div class="box-title">Editar Canal</div>
  </div>
  {!! Form::open(['route' => ['admin.channel.update', $channel->slug], 'method' => 'put']) !!}
    @include('admin.channels.form', ['btn' => 'Salvar'])
  {!! Form::close() !!}
</div>
@stop
