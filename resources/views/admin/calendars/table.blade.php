<table class="table table-hover">
  <tr>
    <th>Imagem</th>
    <th>Nome</th>
    <th>Início</th>
    <th>Fim</th>
    <th>Opções</th>
  </tr>
  @foreach($calendars as $calendar)
    <tr>
      <td>
        <img src="{!! $calendar->image ? $calendar->image->getRelativeUrl() : 'http://placehold.it/50x50' !!}" width="50" height="50">
      </td>
      <td>{!! $calendar->title !!}</td>
      <td>{!! $calendar->start_at !!}</td>
      <td>{!! $calendar->end_at !!}</td>
      <td>
        <a type="button" href="{!! route('admin.calendar.edit', [$calendar->slug]) !!}" class="btn btn-info btn-xs" data-toggle="tooltip" data-container="body" title="Editar"><i class="fa fa-edit"></i></a>
        <a type="button" href="{!! route('admin.calendar.destroy', [$calendar->slug]) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-container="body" title="Excluir"><i class="fa fa-remove"></i></a>
      </td>
    </tr>
  @endforeach
</table>
