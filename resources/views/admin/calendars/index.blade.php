@extends('admin.layout')

@section('title')
Calendários
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header">
    <div class="box-title">
      Calendários <span class="label label-warning">{!! $calendars->total() !!}</span>
    </div>
    <div class="box-tools">
      <a href="{!! route('admin.calendar.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Novo</a>
    </div>
  </div>
  <div class="box-body table-responsive no-padding">
    @include('admin.calendars.table')
  </div>
  <div class="box-footer clearfix">
    {!! $calendars->render() !!}
  </div>
</div>
@stop
