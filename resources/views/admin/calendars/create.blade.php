@extends('admin.layout')

@section('title')
Novo Calendário
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Novo Calendário</div>
  </div>
  {!! Form::open(['route' => ['admin.calendar.store'], 'files' => true]) !!}
    @include('admin.calendars.form', ['btn' => 'Criar'])
  {!! Form::close() !!}
  </form>
</div>
@stop
