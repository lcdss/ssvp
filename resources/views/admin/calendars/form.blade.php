<div class="box-body">
  <fieldset class="col-xs-12 col-sm-6 form-group {!! $errors->has('title') ? 'has-error' : '' !!}{!! ! $errors->has('title') && $errors->any() ? 'has-success' : '' !!}">
    <label class="control-label" for="title">Título</label>
    <input type="text" id="title" name="title" value="{!! $calendar->title or old('title') !!}" class="form-control" required>
  </fieldset>
  <fieldset class="col-xs-12 form-group {!! $errors->has('body') ? 'has-error' : '' !!}{!! ! $errors->has('body') && $errors->any() ? 'has-success' : '' !!}">
    <label class="control-label" for="body">Descrição</label>
    <textarea type="text" id="body" name="body">
      @if (isset($calendar->body))
        {{ $calendar->body }}
      @elseif (old('body') !== null)
        {{ old('body') }}
      @endif
    </textarea>
  </fieldset>
  <fieldset class="col-xs-12 col-sm-6 form-group {!! $errors->has('start_at') ? 'has-error' : '' !!}{!! ! $errors->has('start_at') && $errors->any() ? 'has-success' : '' !!}">
    <label class="control-label" for="start_at">Início</label>
    <input type="text" id="start_at" name="start_at" value="{!! $calendar->start_at or old('start_at') !!}" class="form-control date">
  </fieldset>
  <fieldset class="col-xs-12 col-sm-6 form-group {!! $errors->has('end_at') ? 'has-error' : '' !!}{!! ! $errors->has('end_at') && $errors->any() ? 'has-success' : '' !!}">
    <label class="control-label" for="end_at">Fim</label>
    <input type="text" id="end_at" name="end_at" value="{!! $calendar->end_at or old('end_at') !!}" class="form-control date">
  </fieldset>
  <fieldset class="col-xs-12 col-sm-6 form-group {!! $errors->has('image') ? 'has-error' : '' !!}{!! ! $errors->has('image') && $errors->any() ? 'has-success' : '' !!}">
    <label class="control-label" for="image">Foto</label>
    <input type="file" id="image" name="image" accept="image/png,image/jpeg,image/gif">

    <p class="help-block">A imagem deve estar nos formatos PNG, JPG ou GIF.</p>
    @if (isset($calendar->image))
      <img src="{!! $calendar->image->getRelativeUrl() !!}" alt="{!! $calendar->image->alt !!}" width="250" height="250">
    @endif
  </fieldset>
</div>
<div class="box-footer">
  <button type="submit" class="btn btn-primary">{!! $btn !!}</button>
</div>
