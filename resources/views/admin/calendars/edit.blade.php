@extends('admin.layout')

@section('title')
Editar Calendário
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Editar Calendário</div>
  </div>
  {!! Form::open(['route' => ['admin.calendar.update', $calendar->slug], 'files' => true, 'method' => 'put']) !!}
    @include('admin.calendars.form', ['btn' => 'Salvar'])
  {!! Form::close() !!}
  </form>
</div>
@stop
