@extends('admin.layout')

@section('title')
Editar Usuário
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Editar Usuário</div>
  </div>
  {!! Form::open(['route' => ['admin.user.update', $user->username], 'method' => 'put', 'files' => true]) !!}
  <div class="box-body">
    <div class="row">
      @include('admin.users.form')
      <div class="col-sm-6 form-group {!! ($errors->has('password')) ? 'has-error' : '' !!}">
        <label class="control-label" for="password">Senha</label>
        <input type="password" id="password" name="password" class="form-control" maxlength="16">
      </div>
      <div class="col-sm-6 form-group {!! ($errors->has('password')) ? 'has-error' : '' !!}">
        <label class="control-label" for="password_confirmation">Confirmação de Senha</label>
        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" maxlength="16">
      </div>
    </div>
  </div>
  <div class="box-footer">
    <button type="submit" class="btn btn-primary">Salvar</button>
  </div>
  {!! Form::close() !!}
</div>
@stop
