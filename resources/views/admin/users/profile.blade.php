@extends('admin.layout')

@section('title')
Meu Perfil
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Editar Usuário</div>
  </div>
  {!! Form::open(['route' => ['admin.user.profile.post', $user->username], 'method' => 'put', 'files' => true]) !!}
  <div class="box-body">
    <div class="row">
      @include('admin.users.form')
      <fieldset class="col-sm-6 form-group {!! ($errors->has('current_password')) ? 'has-error' : '' !!}">
        <label class="control-label" for="current_password">Confirmação de Senha</label>
        <input type="password" id="current_password" name="current_password" class="form-control" maxlength="18" required>
      </fieldset>
    </div>
  </div>
  <div class="box-footer">
    <button type="submit" class="btn btn-primary">Salvar</button>
  </div>
  {!! Form::close() !!}
</div>
<div class="box box-warning">
  <div class="box-header with-border">
    <h3 class="box-title">Redefinir Senha</h3>
  </div>
  {!! Form::open(['route' => ['admin.user.password', $user->username], 'method' => 'put']) !!}
  <div class="box-body">
    <div class="row">
      <fieldset class="col-sm-6 col-md-4 form-group {!! ($errors->has('current_password')) ? 'has-error' : '' !!}">
        <label class="control-label" for="current_password">Senha Atual</label>
        <input type="password" id="current_password" name="current_password" class="form-control" maxlength="18" required>
      </fieldset>
      <fieldset class="col-sm-6 col-md-4 form-group {!! ($errors->has('password')) ? 'has-error' : '' !!}">
        <label class="control-label" for="password">Senha</label>
        <input type="password" id="password" name="password" class="form-control" maxlength="18" required>
      </fieldset>
      <fieldset class="col-sm-6 col-md-4 form-group {!! ($errors->has('password_confirmation')) ? 'has-error' : '' !!}">
        <label class="control-label" for="password_confirmation">Confirmação de Senha</label>
        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" maxlength="18" required>
      </fieldset>
    </div>
  </div>
  <div class="box-footer">
    <button type="submit" class="btn btn-primary">Salvar</button>
  </div>
  {!! Form::close() !!}
</div>
@stop
