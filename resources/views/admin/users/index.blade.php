@extends('admin.layout')

@section('title')
Usuários
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header">
    <div class="box-title">
      Usuários <span class="label label-warning">{!! $users->total() !!}</span>
    </div>
    <div class="box-tools">
      <a href="{!! route('admin.user.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Novo</a>
    </div>
  </div>
  <div class="box-body table-responsive no-padding">
    @include('admin.users.table')
  </div>
  <div class="box-footer clearfix">
    {!! $users->render() !!}
  </div>
</div>
@stop
