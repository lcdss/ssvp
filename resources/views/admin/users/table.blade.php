<table class="table table-hover">
  <tr>
    <th>Foto</th>
    <th>Nome</th>
    <th>Nome de Usuário</th>
    <th>Última Visita</th>
    <th>Data de Criacao</th>
    <th>Opções</th>
  </tr>
  @foreach($users as $user)
    <tr>
      <td>
        <img src="{!! isset($user->image) ? $user->getAvatar() : 'uploads/users/default.png' !!}" width="50" height="50">
      </td>
      <td>{!! $user->name !!}</td>
      <td>{!! $user->username !!}</td>
      <td>{!! $user->last_login !!}</td>
      <td>{!! $user->created_at !!}</td>
      <td>
        <a type="button" href="{!! route('admin.user.edit', [$user->username]) !!}" class="btn btn-info btn-xs" data-toggle="tooltip" data-container="body" title="Editar"><i class="fa fa-edit"></i></a>
        <a type="button" href="{!! route('admin.user.destroy', [$user->username]) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-container="body" title="Excluir"><i class="fa fa-remove"></i></a>
    </tr>
  @endforeach
</table>
