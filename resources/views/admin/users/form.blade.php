<div class="col-sm-6 col-md-4 form-group {!! ($errors->has('first_name')) ? 'has-error' : '' !!}{!! (!$errors->has('first_name')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label" for="first_name">Nome</label>
  <input type="text" id="first_name" name="first_name" value="{!! $user->first_name or old('first_name') !!}" class="form-control" maxlength="30" required>
</div>
<div class="col-sm-6 col-md-4 form-group {!! ($errors->has('last_name')) ? 'has-error' : '' !!}{!! (!$errors->has('last_name')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label" for="last_name">Sobrenome</label>
  <input type="text" id="last_name" name="last_name" value="{!! $user->last_name or old('last_name') !!}" class="form-control" maxlength="30" required>
</div>
<div class="col-sm-6 col-md-4 form-group {!! ($errors->has('username')) ? 'has-error' : '' !!}{!! (!$errors->has('username')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label" for="username">Nome de Usuário</label>
  <input type="text" id="username" name="username" value="{!! $user->username or old('username') !!}" class="form-control" maxlength="20" required>
</div>
<div class="col-sm-6 col-md-4 form-group {!! ($errors->has('email')) ? 'has-error' : '' !!}{!! (!$errors->has('email')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label" for="email">Email</label>
  <input type="email" id="email" name="email" value="{!! $user->email or old('email') !!}" class="form-control" maxlength="50" required>
</div>
<div class="col-xs-12 form-group {!! ($errors->has('image')) ? 'has-error' : '' !!}{!! (!$errors->has('image')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label" for="image">Imagem</label>
  <input type="file" id="image" name="image" accept="image/png,image/jpeg,image/gif">

  <p class="help-block">A imagem deve estar nos formatos PNG, JPG ou GIF.</p>
  @if (isset($user->image))
    <img src="{!! $user->getAvatar() !!}" alt="{!! $user->image->alt !!}" width="250" height="250">
  @endif
</div>
