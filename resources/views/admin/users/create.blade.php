@extends('admin.layout')

@section('title')
Novo Usuário
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Novo Usuário</div>
  </div>
  {!! Form::open(['route' => ['admin.user.store'], 'files' => true]) !!}
  <div class="box-body">
    @include('admin.users.form')
    <fieldset class="col-sm-6 form-group {!! ($errors->has('password')) ? 'has-error' : '' !!}">
      <label class="control-label" for="password">Senha</label>
      <input type="password" id="password" name="password" class="form-control" maxlength="16" required>
    </fieldset>
    <fieldset class="col-sm-6 form-group {!! ($errors->has('password')) ? 'has-error' : '' !!}">
      <label class="control-label" for="password_confirmation">Confirmação de Senha</label>
      <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" maxlength="16" required>
    </fieldset>
  </div>
  <div class="box-footer">
    <button type="submit" class="btn btn-primary">Criar</button>
  </div>
  {!! Form::close() !!}
</div>
@stop
