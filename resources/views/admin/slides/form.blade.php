{!! Form::open(['route' => ['admin.slide.store'], 'files' => true]) !!}
<div class="col-md-6 form-group {!! ($errors->has('title')) ? 'has-error' : '' !!}{!! (!$errors->has('title')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label">Título</label>
  <input type="text" name="title" value="{!! isset($slide->title) ? $slide->title : old('title') !!}" class="form-control" maxlength="100" required>
</div>
<div class="col-md-6 form-group {!! ($errors->has('body')) ? 'has-error' : '' !!}{!! (!$errors->has('body')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label">Descrição</label>
  <input type="text" name="body" value="{!! isset($slide->body) ? $slide->body : old('body') !!}" class="form-control" maxlength="100">
</div>
<div class="col-md-6 form-group {!! ($errors->has('image')) ? 'has-error' : '' !!}{!! (!$errors->has('image')) && ($errors->any()) ? 'has-success' : '' !!}">
  <label class="control-label">Imagem</label>
  <input type="file" name="image" accept="image/png,image/jpeg,image/gif" required>

  <p class="help-block">A imagem deve estar nos formatos PNG, JPG ou GIF.</p>
</div>
<div class="col-md-12 form-group">
  <button type="submit" class="btn btn-primary">Criar</button>
</div>
{!! Form::close() !!}
