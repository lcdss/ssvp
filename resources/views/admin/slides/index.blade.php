@extends('admin.layout')

@section('title')
Slides
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header">
    <div class="box-title">
      Slides <span class="label label-warning">{!! $slides->count() !!}</span>
    </div>
  </div>
  @if ($slides->sum('is_published') >= 5)
    <div class="col-xs-12">
      <div class="alert alert-info" role="alert">
        O número máximo de slides ativos foi atingido. Para inserir um novo, favor desativar ou excluir um ou mais slides ativos.
      </div>
    </div>
  @else
  <div class="box-body table-responsive no-padding" disabled>
    @include('admin.slides.form')
  </div>
  @endif
  <div class="box-footer">
    <div class="row">
      @foreach($slides as $slide)
        <div class="col-xs-6 col-sm-4 col-md-3 col-xl-2 popover-item">
          <a title="Opções"
            data-toggle="popover"
            data-placement="top"
            data-content="<form action='{!! route('admin.slide.update', $slide->id) !!}' method='post'><input type='hidden' name='_method' value='put'><input type='hidden' name='_token' value='{!! csrf_token() !!}'><div class='form-group'><input class='form-control' type='text' name='title' value='{{ $slide->title }}'></div><div class='form-group'><input class='form-control' type='text' name='body' value='{{ $slide->body }}'></div><div class='form-group'><button type='submit' class='btn btn-primary'>Salvar</button></div></form><div class='col-xs-6'><a href='{!! route("admin.slide.destroy", $slide->id) !!}' class='center-block'><i class='fa fa-2x fa-close text-danger'></i></a></div><div class='col-xs-6'><a href='{!! route("admin.slide.toggle", $slide->id) !!}' class='center-block'><i class='fa fa-2x fa-{!! $slide->is_published ? 'lock' : 'unlock' !!} text-success'></i></a></div>">
            <img src="{!! $slide->image->getRelativeUrl() !!}" class="slide img-responsive{!! $slide->is_published ? '' : ' slide-inactive' !!}" alt="{{ $slide->image->alt }}">
          </a>
        </div>
      @endforeach
    </div>
  </div>
</div>
@stop
