@extends('admin.layout')

@section('title')
Logs
@stop

@section('content')
  <div class="box box-warning">
    <div class="box-header">
      <div class="box-title">
        Logs <span class="label label-warning">{!! count($logs) !!}</span>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-2 sidebar">
          <div class="list-group">
            @foreach ($files as $file)
              <a href="{!! route('admin.log.index') !!}?l={!! base64_encode($file) !!}" class="list-group-item {!! $current_file !== $file ?: 'llv-active' !!}">
                {{ $file }}
              </a>
            @endforeach
          </div>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-10 table-container">
          @include('admin._partials.logs_table')
          <div>
            <a href="{!! route('admin.log.index') !!}?dl={{ base64_encode($current_file) }}">
              <i class="fa fa-download-alt"></i> Baixar Arquivo
            </a>
            -
            <a id="delete-log" href="{!! route('admin.log.index') !!}?del={{ base64_encode($current_file) }}">
              <i class="fa fa-trash"></i> Excluir Arquivo
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('js')
  <script>
    $(document).ready(function() {
      $('#table-log').DataTable({
        order: [1, 'desc'],
        stateSave: true,

        stateSaveCallback: function (settings, data) {
          window.localStorage.setItem("datatable_logs", JSON.stringify(data));
        },

        stateLoadCallback: function (settings) {
          var data = JSON.parse(window.localStorage.getItem("datatable_logs"));
          if (data) data.start = 0;
          return data;
        }
      });

      $('.table-container').on('click', '.expand', function() {
        $('#' + $(this).data('display')).toggle();
      });

      $('#delete-log').click(function() {
        return confirm('Você tem certeza?');
      });
    });
  </script>
@stop
