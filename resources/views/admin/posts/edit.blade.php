@extends('admin.layout')

@section('title')
Editar Post
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Editar Post</div>
  </div>
  {!! Form::open(['route' => ['admin.post.update', $post->slug], 'files' => true, 'method' => 'put']) !!}
    @include('admin.posts.form', ['btn' => 'Salvar'])
  {!! Form::close() !!}
  </form>
</div>
@stop
