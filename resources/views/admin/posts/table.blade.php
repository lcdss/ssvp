<table class="table table-hover">
  <tr>
    <th>Imagem</th>
    <th>Título</th>
    <th>Autor</th>
    <th>Canal</th>
    <th>Criado em</th>
    <th>Opções</th>
  </tr>
  @foreach($posts as $post)
    <tr>
      <td>
        <img src="{!! $post->image ? $post->image->getRelativeUrl() : 'http://placehold.it/50x50' !!}" width="50" height="50">
      </td>
      <td>{!! $post->title !!}</td>
      <td>{!! $post->user->username !!}</td>
      <td>{!! $post->channel->title !!}</td>
      <td>{!! $post->created_at !!}</td>
      <td>
        <a type="button" href="{!! route('admin.post.edit', [$post->slug]) !!}" class="btn btn-info btn-xs" data-toggle="tooltip" data-container="body" title="Editar"><i class="fa fa-edit"></i></a>
        <a type="button" href="{!! route('admin.post.destroy', [$post->slug]) !!}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-container="body" title="Excluir"><i class="fa fa-remove"></i></a>
      </td>
    </tr>
  @endforeach
</table>
