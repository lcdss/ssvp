@extends('admin.layout')

@section('title')
Posts
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header">
    <div class="box-title">
      Posts <span class="label label-warning">{!! $posts->total() !!}</span>
    </div>
    <div class="box-tools">
      <a href="{!! route('admin.post.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Novo</a>
    </div>
  </div>
  <div class="box-body table-responsive no-padding">
    @include('admin.posts.table')
  </div>
  <div class="box-footer clearfix">
    {!! $posts->render() !!}
  </div>
</div>
@stop
