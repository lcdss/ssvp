<div class="box-body">
  <fieldset class="col-xs-12 col-sm-6 form-group {!! ($errors->has('title')) ? 'has-error' : '' !!}{!! (!$errors->has('title')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="title">Título</label>
    <input type="text" id="title" name="title" value="{!! isset($post->title) ? $post->title : old('title') !!}" class="form-control" required>
  </fieldset>
  <fieldset class="col-xs-12 col-sm-6 form-group {!! ($errors->has('channel_id')) ? 'has-error' : '' !!}{!! (!$errors->has('channel_id')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="channel_id">Canal</label>
    {!! Form::select('channel_id', $channels, isset($post) ? $post->channel->id : null, ['placeholder' => 'Selecione um canal', 'class' => 'form-control', 'required']) !!}
  </fieldset>
  <fieldset class="col-xs-12 form-group {!! ($errors->has('body')) ? 'has-error' : '' !!}{!! (!$errors->has('body')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="body">Conteúdo</label>
    <textarea type="text" id="body" name="body">
      @if (isset($post->body))
        {{ $post->body }}
      @elseif (old('body') !== null)
        {{ old('body') }}
      @endif
    </textarea>
  </fieldset>
  <fieldset class="col-xs-12 col-sm-6 form-group {!! ($errors->has('image')) ? 'has-error' : '' !!}{!! (!$errors->has('image')) && ($errors->any()) ? 'has-success' : '' !!}">
    <label class="control-label" for="image">Imagem</label>
    <input type="file" id="image" name="image" accept="image/png,image/jpeg,image/gif">

    <p class="help-block">A imagem deve estar nos formatos PNG, JPG ou GIF.</p>
    @if (isset($post->image))
      <img src="{!! $post->image->getRelativeUrl() !!}" alt="{!! $post->image->alt !!}" width="250" height="250">
    @endif
  </fieldset>
  <fieldset class="col-xs-12 col-sm-6 form-group {!! ($errors->has('is_published')) ? 'has-error' : '' !!}{!! (!$errors->has('is_published')) && ($errors->any()) ? 'has-success' : '' !!}">
    <div class="checkbox">
      <label class="control-label" for="is_published">
        <input type="checkbox" id="is_published" name="is_published" {!! isset($post->is_published) && $post->is_published ? 'checked' : '' !!}>
        Publicar
      </label>
    </div>
  </fieldset>
</div>
<div class="box-footer">
  <button type="submit" class="btn btn-primary">{!! $btn !!}</button>
</div>
