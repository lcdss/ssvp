@extends('admin.layout')

@section('title')
Novo Post
@stop

@section('content')
<div class="box box-warning">
  <div class="box-header with-border">
    <div class="box-title">Novo Post</div>
  </div>
  {!! Form::open(['route' => ['admin.post.store'], 'files' => true]) !!}
    @include('admin.posts.form', ['btn' => 'Criar'])
  {!! Form::close() !!}
  </form>
</div>
@stop
