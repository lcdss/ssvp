<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <base href="{!! asset('/') !!}">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="author" content="Geartec">
  <meta name="description" content="">
  <meta name="keywords" content="">

  <title>@section('title')@show | Painel de Controle | SSVP</title>

  <link rel="icon" href="favicon.png">

  <link rel="stylesheet" href="css/vendor.css">
  <link rel="stylesheet" href="css/app.css">
  <link rel="stylesheet" href="css/skins/skin-{!! config('cms.skin') !!}.css" id="skin">
  <link rel="stylesheet" href="//cdn.datatables.net/plug-ins/1.10.10/integration/font-awesome/dataTables.fontAwesome.css">

  @yield('css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-{!! config('cms.skin') !!} sidebar-mini">
  <div class="wrapper">
    @include('admin._partials.header')

    @include('admin._partials.sidebar')

    <div class="content-wrapper">
      @if (Breadcrumbs::exists(Request::route()->getName()))
        <section class="content-header">
          <h5>{!! Breadcrumbs::render(Request::route()->getName()) !!}</h5>
        </section>
      @endif

      <section class="content">
        @include('vendor.flash.message')
        @include('partials.errors')
        @yield('content')
      </section>
    </div>

    {{-- @include('admin._partials.footer') --}}

    @include('admin._partials.settings')

    <div class="control-sidebar-bg"></div>
  </div>

  <script src="js/vendor.js"></script>
  <script src="js/app.js"></script>
  @yield('js')
</body>
</html>
