<header class="main-header">
  <a href="{{ route('site.index') }}" class="logo">
    <span class="logo-mini"><b>SVP</b></span>
    <span class="logo-lg"><b>SSVP</b></span>
  </a>
  <nav class="navbar navbar-static-top" role="navigation">
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{!! Auth::user()->getAvatar() !!}" class="user-image" alt="User Image">
            <span class="hidden-xs">{!! Auth::user()->username !!}</span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
              <img src="{!! Auth::user()->getAvatar() !!}" class="img-circle" alt="User Image">
              <p>{!! Auth::user()->getFullName() !!}</p>
            </li>
            <li class="user-footer">
              <div class="pull-left">
                <a href="{!! route('admin.user.profile.get', Auth::user()->username) !!}" class="btn btn-default btn-flat">Perfil</a>
              </div>
              <div class="pull-right">
                <a href="{{ route('auth.logout') }}" class="btn btn-default btn-flat">Sair</a>
              </div>
            </li>
          </ul>
        </li>
        <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>

  </nav>
</header>
