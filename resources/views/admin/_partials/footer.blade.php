<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 0.1.5
  </div>
  <strong>Copyright &copy; 2015 <a href="http://www.geartec.com.br">Geartec</a>.</strong> Todos os direitos
  reservados.
</footer>
