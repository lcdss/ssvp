<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{!! Auth::user()->getAvatar() !!}" class="img-circle" width="45" height="45" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><a href="{!! route('admin.user.edit', Auth::user()->username) !!}">{!! Auth::user()->username !!}</a>
        </p>
      </div>
    </div>
    <ul class="sidebar-menu">
      @role('admin')
      <li class="treeview{!! Request::is('admin') ? ' active' : '' !!}">
        <a href="{!! route('admin.index') !!}">
          <i class="fa fa-dashboard"></i> <span>Painel</span>
        </a>
      </li>
      @endrole
      <li class="treeview{!! Request::is('admin/posts', 'admin/posts/*') ? ' active' : '' !!}">
        <a href="{!! route('admin.post.index') !!}">
          <i class="fa fa-thumb-tack"></i> <span>Posts</span>
        </a>
      </li>
      <li class="treeview{!! Request::is('admin/galleries', 'admin/galleries/*') ? ' active' : '' !!}">
        <a href="{!! route('admin.gallery.index') !!}">
          <i class="fa fa-image"></i> <span>Galerias</span>
        </a>
      </li>
      <li class="treeview{!! Request::is('admin/calendars', 'admin/calendars/*') ? ' active' : '' !!}">
        <a href="{!! route('admin.calendar.index') !!}">
          <i class="fa fa-calendar"></i> <span>Calendários</span>
        </a>
      </li>
      <li class="treeview{!! Request::is('admin/slides', 'admin/slides/*') ? ' active' : '' !!}">
        <a href="{!! route('admin.slide.index') !!}">
          <i class="fa fa-sliders"></i> <span>Slides</span>
        </a>
      </li>
      <li class="treeview{!! Request::is('admin/saints', 'admin/saints/*') ? ' active' : '' !!}">
        <a href="{!! route('admin.saint.index') !!}">
          <i class="fa fa-child"></i> <span>Santos</span>
        </a>
      </li>
      @role(['owner', 'admin'])
        <li class="treeview{!! Request::is('admin/users', 'admin/users/*') ? ' active' : '' !!}">
          <a href="{!! route('admin.user.index') !!}">
            <i class="fa fa-users"></i> <span>Usuários</span>
          </a>
        </li>
      @endrole
      @role('admin')
        <li class="treeview{!! Request::is('admin/channels', 'admin/channels/*') ? ' active' : '' !!}">
          <a href="{!! route('admin.channel.index') !!}">
            <i class="fa fa-newspaper-o"></i> <span>Canais</span>
          </a>
        </li>
        <li class="treeview{!! Request::is('admin/logs', 'admin/logs/*') ? ' active' : '' !!}">
          <a href="{!! route('admin.log.index') !!}">
            <i class="fa fa-calendar"></i> <span>Logs</span>
          </a>
        </li>
      @endrole
      <!-- <li class="treeview">
        <a href="#">
          <i class="fa fa-book"></i> <span>Regras</span>
        </a>
      </li> -->
    </ul>
  </section>
</aside>
