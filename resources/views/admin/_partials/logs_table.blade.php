@if ($logs === null)
  <div>
    Arquivo > 50M, favor baixa-lo.
  </div>
@else
  <table id="table-log" class="table table-striped">
    <thead>
      <tr>
      <th>Nível</th>
      <th>Data</th>
      <th>Conteúdo</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($logs as $key => $log)
      <tr>
        <td class="text-{!! $log['level_class'] !!}">
          <i class="fa fa-{!! $log['level_img'] !!}-sign" aria-hidden="true"></i> &nbsp;{!! $log['level'] !!}
        </td>
        <td class="log-date">{!! $log['date'] !!}</td>
        <td class="log-text">
          @if ($log['stack'])
            <a class="pull-right expand btn btn-default btn-xs" data-display="stack{!! $key !!}">
              <i class="fa fa-search"></i>
            </a>
          @endif
          {{ $log['text'] }}
          @if (isset($log['in_file']))
            <br>{{ $log['in_file'] }}
          @endif
          @if ($log['stack'])
          <div class="log-stack" id="stack{{ $key }}" style="display: none; white-space: pre-wrap;">
            {{ trim($log['stack']) }}
          </div>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endif
