@extends('site.layouts.main')

@section('title')
Santos
@stop

@section('content')
<section class="row box box-solid no-padding">
  <header class="box-header title-header">
    <span class="title-text">Santos</span>
  </header>
  <div class="box-body">
    <ul class="event-list">
      @foreach($saints as $saint)
      <div class="col-xs-12 col-lg-4">
        <li>
          <time datetime="{{ $saint->date }}">
            <span class="day">{{ $saint->day }}</span>
            <span class="month">{{ $saint->month }}</span>
            <span class="year">{{ $saint->year }}</span>
          </time>
          <div class="info">
            <h4 class="title"><a href="{!! route('site.saint.show', $saint->slug) !!}">{{ $saint->name }}</a></h4>
          </div>
        </li>
      </div>
      @endforeach
    </ul>
  </div>
  <footer class="box-footer">
    {!! $saints->render() !!}
  </footer>
</section>
@stop
