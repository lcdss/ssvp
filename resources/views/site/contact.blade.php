@extends('site.layouts.main')

@section('title')
Contato
@stop

@section('content')
  <section class="row box box-solid no-padding">
    <article>
      <header class="box-header title-header">
        <span class="title-text">Contato</span>
      </header>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m8!1m3!1d1914.4257316287237!2d-48.94001514351118!3d-16.330532134239387!3m2!1i1024!2i768!4f13.1!4m12!1i0!3e6!4m5!1s0x935ea6af8e240293%3A0xd9d82539ad778cb8!2sCol%C3%A9gio+Galileu+-+Avenida+Santos+Dumont%2C+724+-+Jundia%C3%AD%2C+An%C3%A1polis+-+GO%2C+75113-180%2C+Brazil!3m2!1d-16.329725!2d-48.941413999999995!4m3!3m2!1d-16.3316296!2d-48.9393226!5e0!3m2!1spt-BR!2s!4v1399768606853" width="100%" height="350" frameborder="0"></iframe>
          </div>
          <div class="col-md-12">
            @include('vendor.flash.message')
            <form action="{!! route('site.contact.post') !!}" method="post">
              {!! csrf_field() !!}
              <div class="row">
                <div class="form-group col-md-6">
                  <label>Nome:</label>
                  <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                </div>
                <div class="form-group col-md-6">
                  <label>E-mail:</label>
                  <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                </div>
                <div class="form-group col-md-12">
                  <label>Assunto:</label>
                  <input type="text" class="form-control" name="subject" value="{{ old('subject') }}" required>
                </div>
                <div class="form-group col-md-12">
                  <label>Mensagem:</label>
                  <textarea class="form-control resize-v min-height-150" name="body" rows="3" required>{{ old('body') }}</textarea>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-flat pull-right">Enviar</button>
            </form>
          </div>
        </div>
      </div>
    </article>
  </section>
@stop
