<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <base href="{!! asset('/') !!}">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Geartec">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>@section('title')@show | Sociedade São Vicente de Paula | SSVP</title>

    <link href="favicon.png" rel="icon">
    <link href="css/vendor.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">

    @yield('css')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="top">
    </div>
    <header>
      @include('site.partials.header')
    </header>
    <div id="page" class="content">
      <main class="col-xs-12 col-sm-8">
        @include('partials.errors')
        @yield('content')
      </main>
      <aside class="col-xs-12 col-sm-4">
        @include('site.partials.sidebar')
      </aside>
    </div>
    <footer>
      @include('site.partials.footer')
    </footer>
    <script src="js/vendor.js"></script>
    <script src="js/app.js"></script>

    @yield('js')

    @if (env('APP_ENV') == 'production')
      @include('site.partials.analytics')
    @endif
  </body>
</html>
