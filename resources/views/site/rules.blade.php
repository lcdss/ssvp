@extends('site.layouts.main')

@section('title')
Regras
@stop

@section('content')
  <section class="row box box-solid no-padding">
    <article>
      <header class="box-header title-header">
        <span class="title-text">A Regra da Sociedade São Vicente de Paula</span>
      </header>
      <div class="box-body">
        <img class="pull-left img-responsive img-article margin-r-5" src="/img/rule.jpg" alt="">
        <p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laborum tempore expedita incidunt quidem sequi at saepe, quasi hic officiis dolorem aliquid culpa velit, maiores consequuntur facilis ipsa molestias? Repellat.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laborum tempore expedita incidunt quidem sequi at saepe, quasi hic officiis dolorem aliquid culpa velit, maiores consequuntur facilis ipsa molestias? Repellat.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laborum tempore expedita incidunt quidem sequi at saepe, quasi hic officiis dolorem aliquid culpa velit, maiores consequuntur facilis ipsa molestias? Repellat.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laborum tempore expedita incidunt quidem sequi at saepe, quasi hic officiis dolorem aliquid culpa velit, maiores consequuntur facilis ipsa molestias? Repellat.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laborum tempore expedita incidunt quidem sequi at saepe, quasi hic officiis dolorem aliquid culpa velit, maiores consequuntur facilis ipsa molestias? Repellat.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse laborum tempore expedita incidunt quidem sequi at saepe, quasi hic officiis dolorem aliquid culpa velit, maiores consequuntur facilis ipsa molestias? Repellat.Esse laborum tempore expedita incidunt quidem sequi at saepe, quasi hic officiis dolorem aliquid culpa velit, maiores consequuntur facilis ipsa molestias? Repellat.</p>
        <footer class="col-xs-12 box-footer text-right">
          <a href="{!! route('site.rules.download') !!}" class="btn btn-warning btn-flat pull-right">Baixar</a>
        </footer>
      </div>
    </article>
  </section>
@stop
