@extends('site.layouts.main')

@section('title')
Canal | {{ $channel->title }}
@stop

@section('content')
<section class="row box box-solid no-padding">
  <article>
    <header class="box-header title-header">
      <span class="title-text">{{ $channel->title }}</span>
    </header>
    <div class="box-body">
      @forelse ($posts = $channel->posts()->paginate(10) as $post)
        <div class="notice notice-info">
          @if ($post->image)
            <img src="{{ $post->image->getRelativeUrl() }}" width="50" alt="{!! $post->image->alt !!}">
          @endif
          <a href="{!! route('site.post', [$channel->slug, $post->slug]) !!}">&nbsp;&nbsp;<strong>{{ $post->title }}</strong></a>
        </div>
      @empty
        Não há nenhum post neste canal.
      @endforelse
    </div>
    <div class="col-xs-12">
      {!! $posts->render() !!}
    </div>
  </article>
</section>
@stop
