@extends('site.layouts.main')

@section('title')
Galerias de Fotos
@stop

@section('content')
<section class="row box box-solid no-padding">
  <article>
    <header class="box-header title-header">
      <span class="title-text">Nossas Galerias de Fotos</span>
    </header>
    <div class="box-body">
      @foreach ($galleries as $gallery)
        @if (isset($gallery->images) && count($gallery->images) > 0)
          <?php $image = $gallery->getRandomImage(); ?>
          <div class="album-item col-md-4">
            <img class="cover-center margin" src="{!! $image->getRelativeUrl() !!}" alt="{{ $image->alt }}">
            <div class="overlay">
              <div class="content">
                <a href="{!! route('site.gallery.show', $gallery->slug) !!}" class="plus">+</a>
                <p class="body">{{ str_limit($gallery->body, 100) }}</p>
              </div>
            </div>
          </div>
        @endif
      @endforeach
    </div>
  </article>
</section>
@stop
