@extends('site.layouts.main')

@section('title')
{{ $saint->name }} | Santos
@stop

@section('content')
  <section class="row box box-solid no-padding">
    <article>
      <header class="box-header title-header">
        <span class="title-text">{{ $saint->name }}</span>
      </header>
      <div class="box-body">
        <img class="pull-left img-responsive img-article margin-r-5" src="{!! $saint->image->getRelativeUrl() !!}" alt="">
        <span class="direct-chat-timestamp">Data: {{ $saint->date }}</span>
        <p class="text-justify">{!! $saint->body !!}</p>
      </div>
    </article>
  </section>
@stop
