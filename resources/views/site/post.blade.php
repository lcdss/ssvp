@extends('site.layouts.main')

@section('title')
{{ $post->title }} | {{ $channel->title }}
@stop

@section('content')
<section class="row box box-solid no-padding">
  <article>
    <header class="box-header title-header">
      <span class="title-text">{{ $post->title }}</span>
    </header>
    <div class="box-body">
      @if ($post->image)
        <img class="pull-left img-responsive img-article margin-r-5" src="{{ $post->image->getRelativeUrl() }}" alt="{!! $post->image->alt !!}">
      @endif
      <p class="text-justify">{!! $post->body !!}</p>
    </div>
  </article>
</section>
@stop
