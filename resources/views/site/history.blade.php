@extends('site.layouts.main')

@section('title')
Historia SSVP
@stop

@section('content')
<section class="row box box-solid no-padding">
  <article>
    <header class="box-header title-header">
      <span class="title-text">História da SSVP</span>
    </header>
    <div class="box-body">
      <p class="text-justify">
        Conhecida pelas iniciais SSVP, a Sociedade de São Vicente de Paulo  foi fundada em abril de 1833, em Paris, por um grupo de sete jovens universitários liderados por Frédéric Antoine Ozanam (1813-1853), estudante de Direito na Universidade de Sorbonne, um jovem, na época com apenas 20 anos, de idade e alguns companheiros. A organização adotou São Vicente de Paulo (1581-1660) como patrono, inspirando-se no pensamento e na obra daquele santo, conhecido como o Pai da Caridade pela sua dedicação ao serviço dos pobres e dos infelizes.
      </p>
      <p class="text-justify">
        A SSVP tem a preocupação de renovar-se constantemente e adaptar-se às condições mutáveis do mundo. De caráter católico, está aberta a quantos desejam viver sua fé no amor e no serviço a seus irmãos. A unidade da SSVP no mundo é representada por sua REGRA (REGULAMENTO). Busca, incansavelmente, um trabalho de maior contato e aproximação com a Igreja, através do Clero.
      </p>
      <p class="text-justify">
        A coordenação do trabalho vicentino depende de uma organização simples, mas complexa: primeiro existem grupos, tradicionalmente chamados de Conferências, que se reúnem com regularidade e freqüência. Essas <strong>Conferências</strong> são unidas entre si por meio de <strong>Conselhos Particulares</strong>, de âmbito local. Esses são vinculados a <strong>Conselhos Centrais</strong>, órgãos executivos em determinada circunscrição. Na seqüência hierárquica há os <strong>Conselhos Metropolitanos</strong>, de âmbito regional. Em nível nacional existe o <strong>Conselho Nacional do Brasil</strong>, com sede no Rio de Janeiro, RJ. Coordenando o trabalho em todo mundo está o <strong>Conselho Geral Internacional</strong>, em Paris, na França.
      </p>
      <p class="text-justify">
        Atualmente a SSVP está presente em 146 (cento e quarenta e seis) países, com 720.000 (setecentos e vinte mil) membros. O Brasil é o maior país vicentino do mundo: são 45.440 (quarenta e cinco mil, quatrocentos e quarenta) Conferências, mais de 2 mil Conselhos Particulares, quase 300 Conselhos Centrais, 33 Conselhos Metropolitanos, e um Conselho Nacional e milhares de Obras Unidas e Especiais, subordinadas ao Conselho Geral Internacional.
      </p>
      <p class="text-justify">
        A primeira Conferência do Brasil
      </p>
      <p class="text-justify">
        Segundo Lopez (1991), na década de 70, do século XIX, o Brasil passava por mudanças, ou seja, o Brasil saía de uma guerra, a Guerra do Paraguai, e foi dentro desse contexto que nasceu a primeira conferência vicentina no Brasil para tentar amenizar a situação dos pobres e injustiçados, como já estava sendo feito em outros países.
      </p>
      <p class="text-justify">
        No dia 19 de julho de 1872, alguns leigos foram convidados pelos padres lazaristas para jantar no seminário diocesano de São José, isso aconteceu depois de uma cerimônia religiosa em louvor a São Vicente de Paulo na Capela da Santa Casa de Misericórdia do Rio de Janeiro. No jantar estavam presentes, entre outros, o conceituado médico Dr. Pedro Fortes Marcordes Jabim, o advogado Antônio Socioso Moreira de Sá e Francisco Lemos Faria Coutinho - um fidalgo a serviço da imperatriz do Brasil D. Leopoldina. E foi nesse jantar que um dos presentes ficou surpreso em saber que ainda não existia a SSVP no Brasil, um grande país católico, onde os menos favorecidos reclamavam por ações de caridade. No dia 04 de agosto de 1872 era implantada a primeira conferência em terras brasileiras, a conferência São José. Ela foi agregada em 16 de novembro de 1872, sendo essa data a oficial da implantação da SSVP no Brasil. Na primeira reunião o conde de Aljesur que era o fundador da primeira conferência em Portugal foi eleito presidente para nomear os confrades Pedro Jobim para secretário e Antônio Secioso para tesoureiro.
      </p>
    </div>
  </article>
</section>
@stop
