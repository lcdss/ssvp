<div class="row box box-solid">
  <header class="box-header title-header"><span class="title-text">Próximos Compromissos</span></header>
  <div class="box-body col-xs-12">
    @include('site.partials.calendar')
  </div>
</div>

<div class="row box box-solid">
  <header class="box-header title-header"><span class="title-text">Links Uteis</span></header>
  <div class="box-body col-xs-12">
    <ul class="nav nav-pills nav-stacked">
      <li><a target="_blank" href="http://www.ssvpbrasil.org.br/"><i class="fa fa-circle-o text-light-blue"></i> SSVP Brasil</a></li>
      <li><a target="_blank" href="http://www.ssvpcmb.org.br"><i class="fa fa-circle-o text-light-blue"></i> SSVP Brasilia</a></li>
      <li><a target="_blank" href="http://ssvpcmbh.org.br"><i class="fa fa-circle-o text-light-blue"></i> SSVP Belo HorizonteVP</a></li>
      <li><a target="_blank" href="https://conselho.wordpress.com"><i class="fa fa-circle-o text-light-blue"></i> SSVP Porto Alegre</a></li>
      <li><a target="_blank" href="http://cmcuritiba.org.br/"><i class="fa fa-circle-o text-light-blue"></i> SSVP Curitiba</a></li>
      <li><a target="_blank" href="http://www.ssvpformosa.org.br/"><i class="fa fa-circle-o text-light-blue"></i> SSVP Formosa</a></li>
      <li><a target="_blank" href="http://www.ssvpsp.org"><i class="fa fa-circle-o text-light-blue"></i> SSVP Botucatu SP</a></li>
    </ul>
  </div>
</div>
