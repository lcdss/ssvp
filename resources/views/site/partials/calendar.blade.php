<ul class="event-list">
  @foreach ($nextCalendars as $calendar)
    <li>
      <time datetime="{{ $calendar->created_at }}">
        <span class="day">{{ $calendar->day }}</span>
        <span class="month">{{ $calendar->month }}</span>
        <span class="year">{{ $calendar->year }}</span>
        <span class="time">{{ $calendar->time }}</span>
      </time>

      <div class="info">
        <h5 class="title"><a href="{!! route('site.calendar', $calendar->slug) !!}">{{ $calendar->title }}</a></h5>
      </div>
    </li>
  @endforeach
</ul>
