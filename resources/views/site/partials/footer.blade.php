<div class="container-fluid copyright-bar">
  <div class="row">
    <div class="col-xs-12">
      <div class="row">
        <div class="col-md-8 col-xs-12">
          <p>
            © COPYRIGHT 2015 - <strong>Sociedade São Vicente de Paula</strong> - TODOS OS DIREITOS RESERVADOS.
          </p>
        </div>
        <div class="col-md-4 col-xs-12">
          <div class="row">
            <div class="col-md-7 col-xs-12 pull-right">
              <span>Desenvolvido por: </span>
              <a href="//www.geartec.com.br/" target="_blank">
                <img src="/img/geartec.png" alt="GearTec" title="GearTec">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
