<div id="carousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    @foreach ($slides as $key => $slide)
      <div class="item {!! $key != 0 ?: 'active' !!}">
        <img src="{!! $slide->image->getRelativeUrl() !!}" alt="{{ $slide->image->alt }}">
        @if (! empty($slide->title))
          <div class="carousel-caption">
            <h4>{{ $slide->title }}</h4>

            @if (! empty($slide->body))
              <p>{{ $slide->body }}</p>
            @endif
          </div>
        @endif
      </div>
    @endforeach
  </div>

  <ul class="list-group products-list col-sm-4">
    @foreach ($slides as $key => $slide)
      <li data-target="#carousel" data-slide-to="{!! $key !!}" class="list-group-item {!! $key != 0 ?: 'active' !!}">
        <h4>{{ $slide->title }}</h4>
      </li>
    @endforeach
  </ul>

  <div class="carousel-controls">
    <a class="left carousel-control" href="#carousel" data-slide="prev">
      <span class="fa fa-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel" data-slide="next">
      <span class="fa fa-chevron-right"></span>
    </a>
  </div>
</div>
