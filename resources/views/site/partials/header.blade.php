<div id="logo">
  <div class="logo-title-center">
    <div class="logo-title">Conselho Metropolitano de Anápolis</div>
  </div>
</div>

<nav class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-main" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <div class="collapse navbar-collapse" id="navbar-collapse-main">
    <ul class="nav navbar-nav" id="site-navbar-nav">
      <li class="{!! URL::current() != route('site.index') ?: 'active' !!}">
        <a href="{!! route('site.index') !!}">Início</a>
      </li>
      <li class="dropdown{!! Request::is('canais/*') && ! (starts_with(Request::segment(2), 'noticias') || starts_with(Request::segment(2), 'palavra')) ? ' active' : '' !!}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">  Canais <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
          @foreach ($channels as $channel)
            <li class="{!! URL::current() != route('site.channel', $channel->slug) ?: 'active' !!}">
              <a href="{!! route('site.channel', $channel->slug) !!}">{{ $channel->title }}</a>
            </li>
          @endforeach
        </ul>
      </li>
      <li class="dropdown{!! Request::is('canais/*') && (starts_with(Request::segment(2), 'noticias') || starts_with(Request::segment(2), 'palavra')) ? ' active' : '' !!}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">  Notícias <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
          @foreach ($news as $new)
            <li class="{!! URL::current() != route('site.channel', $new->slug) ?: 'active' !!}">
              <a href="{!! route('site.channel', $new->slug) !!}">{{ $new->title }}</a>
            </li>
          @endforeach
        </ul>
      </li>
      <li class="{!! URL::current() != route('site.calendar') ?: 'active' !!}">
        <a href="{!! route('site.calendar') !!}">Calendário</a>
      </li>
      <li class="{!! URL::current() != route('site.gallery') ?: 'active' !!}">
        <a href="{!! route('site.gallery') !!}">Galerias de Fotos</a>
      </li>
      <li class="{!! URL::current() != route('site.saint') ?: 'active' !!}">
        <a href="{!! route('site.saint') !!}">Santos</a>
      </li>
      <li class="{!! URL::current() != route('site.director') ?: 'active' !!}">
        <a href="{!! route('site.director') !!}">Diretoria</a>
      </li>
      <li class="{!! URL::current() != route('site.rules') ?: 'active' !!}">
        <a href="{!! route('site.rules') !!}">Regras</a>
      </li>
      <li class="{!! URL::current() != route('site.history') ?: 'active' !!}">
        <a href="{!! route('site.history') !!}">Historia</a>
      </li>
      </li>
      <li class="{!! URL::current() != route('site.contact') ?: 'active' !!}">
        <a href="{!! route('site.contact') !!}">Contato</a>
      </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
      @if(Auth::check())
        <li class="{!! URL::current() != route('admin.index') ?: 'active' !!}">
          <a href="{!! route('admin.index') !!}">Admin</a>
        </li>
      @else
        <li class="{!! URL::current() != route('auth.login.get') ?: 'active' !!}">
          <a href="{!! route('auth.login.get') !!}">Login</a>
        </li>
      @endif
    </ul>
  </div>
</nav>
