@extends('site.layouts.main')

@section('title')
Diretoria
@stop

@section('content')
<section class="row box box-solid no-padding">
  <header class="box-header title-header">
    <span class="title-text">Diretoria do CMA</span>
  </header>
  <div class="box-body">
    <div class="row">
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Presidente:</span>
          <p>João Modesto Pereira</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> presidente@ssvpcma.com</p>
        </div>
      </div>
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Vice-Presidente:</span>
          <p>Orlando de Oliveira Machado</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> vice.presidente@ssvpcma.com</p>
        </div>
      </div>
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Secretária:</span>
          <p>Joanarosa Stival Teozilo</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> secretaria@ssvpcma.com</p>
        </div>
      </div>
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Tesoureiro:</span>
          <p>Paulo Antônio Nogueira</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> presidente@ssvpcma.com</p>
        </div>
      </div>
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Coordenadora da Comissão De Jovens:</span>
          <p>Gilzélia De Morais Arantes Lima</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> presidente@ssvpcma.com</p>
        </div>
      </div>
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Ecafo:</span>
          <p>Miguel Alves Santana</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> presidente@ssvpcma.com</p>
        </div>
      </div>
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Coordenador De Crianças E Adolescentes:</span>
          <p>Lorrany Rego de Jesus</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> presidente@ssvpcma.com</p>
        </div>
      </div>
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Denor:</span>
          <p>José Eustaquio Vieira</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> presidente@ssvpcma.com</p>
        </div>
      </div>
      <div class="items col-xs-12 col-sm-6 clearfix">
        <div class="info-box clearfix">
          <img class="margin-r-5 pull-left img-responsive" src="http://imgholder.ru/100x110/f78b02/ECEFF1.png" alt="">
          <span>Decom:</span>
          <p>Jalles Leonardo Morais Santos</p>
          <p><i class="fa fa-phone"></i> 555-555-5555</p>
          <p><i class="fa fa-envelope-o"></i> presidente@ssvpcma.com</p>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
