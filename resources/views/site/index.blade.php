@extends('site.layouts.main')

@section('title')
Início
@stop

@section('content')
<div class="row box box-solid">
  @include('site.partials.carousel')
</div>

<div class="row no-padding">
  <div class="col-md-8">
    <section class="row box box-solid no-padding">
      <header class="box-header title-header">
        <span class="title-text">Notícias Metropolitanas</span>
      </header>
      @foreach ($metropolitanNews as $metropolitan)
        <article class="col-md-6">
          <img class="img-responsive center-block" src="{{ $metropolitan->image->getRelativeUrl() }}" alt="">
          <header class="text-yellow">{{ $metropolitan->title }}</header>
          <p class="text-justify">{!! str_limit($metropolitan->body, 250) !!}</p>
          <footer class="box-footer text-right">
            <a href="{!! route('site.post', ['noticias-metropolitanas', $metropolitan->slug]) !!}" class="uppercase">Leia Mas ...</a>
          </footer>
        </article>
      @endforeach
    </section>
  </div>
  <div class="col-md-4 pd-l-3px">
    <section class="row box box-solid no-padding">
      <header class="box-header title-header"><span class="title-text">Palavra do Presidente</span></header>
      <article class="col-md-12">
        <img class="img-responsive center-block" src="{{ $presidentWord->image->getRelativeUrl() }}" alt="">
        <header class="text-yellow">{{ $presidentWord->title }}</header>
        <p class="text-justify">{!! str_limit($presidentWord->body, 250) !!}</p>
        <footer class="box-footer text-right">
          <a href="{!! route('site.post', ['palavra-do-presidente', $presidentWord->slug]) !!}" class="uppercase">Leia Mas ...</a>
        </footer>
      </article>
    </section>
  </div>

  <div class="col-md-12">
    <section class="row box box-solid no-padding">
      <article class="col-md-8 no-padding">
        <header class="box-header title-header"><span class="title-text">Notícias Canais</span></header>
        <div class="box-body">
          <img class="pull-left img-responsive img-article margin-r-5 margin-bottom" src="{{ $channelNewsFirst->image->getRelativeUrl() }}" alt="{{ $channelNewsFirst->image->alt }}">
          <header class="text-yellow">{{ $channelNewsFirst->title }}</header>
          <p class="text-justify">{!! str_limit($channelNewsFirst->body, 500) !!}</p>
          <footer class="col-xs-12 box-footer text-right">
            <a href="{!! route('site.post', [$channelNewsFirst->channel->slug, $channelNewsFirst->slug]) !!}" class="uppercase">Leia Mas ...</a>
          </footer>
        </div>
      </article>
      <article class="col-md-4 no-padding">
        <header class="box-header title-header"><span class="title-text">Mais Notícias</span></header>
        <div class="box-body">
          <ul class="products-list product-list-in-box">
            @foreach ($channelNews as $channelNew)
              <li class="item">
                <div class="product-img">
                  <img class="img-responsive img-article" src="{{ $channelNew->image->getRelativeUrl() }}" alt="{{ $channelNew->image->alt }}">
                </div>
                <div class="product-info">
                  <a class="text-muted" href="{!! route('site.post', [$channelNew->channel->slug, $channelNew->slug]) !!}" class="product-title default">{{ $channelNew->title }}</a>
                </div>
              </li>
            @endforeach
          </ul>
        </div>
      </article>
    </section>
  </div>

  <div class="col-md-{!! is_null($saintOfTheDay) ? '12' : '8' !!}">
    <section class="row box box-solid no-padding">
      <header class="box-header title-header">
        <span class="title-text">Notícias</span>
      </header>
      @foreach ($news as $new)
      <article class="col-md-6">
        <img class="img-responsive img-article center-block" src="{{ $new->image->getRelativeUrl() }}" alt="{{ $new->image->alt }}">
        <header class="text-yellow">{{ $new->title }}</header>
        <p class="text-justify">{!! str_limit($new->body, 250) !!}</p>
        <footer class="box-footer text-right">
          <a href="{!! route('site.post', ['noticias', $new->slug]) !!}" class="uppercase">Leia Mas ...</a>
        </footer>
      </article>
      @endforeach
    </section>
  </div>
  @if (! is_null($saintOfTheDay))
    <div class="col-md-4 pd-l-3px">
      <section class="row box box-solid no-padding">
        <header class="box-header title-header"><span class="title-text">Santo do Dia</span></header>
        <article class="col-md-12">
          <img class="img-responsive img-article center-block" src="{!! $saintOfTheDay->image->getRelativeUrl() !!}" alt="{{ $saintOfTheDay->image->alt }}">
          <header class="text-yellow">{{ $saintOfTheDay->name }}</header>
          <p class="text-justify">{!! str_limit($saintOfTheDay->body, 250) !!}</p>
          <footer class="box-footer text-right">
            <a href="{!! route('site.saint.show', $saintOfTheDay->slug) !!}" class="uppercase">Leia Mais...</a>
          </footer>
        </article>
      </section>
    </div>
  @endif
</div>
@stop
