@extends('site.layouts.main')

@section('title')
{{ $firstCalendar->title }}
@stop

@section('content')
<section class="row box box-solid no-padding">
  <article>
    <header class="box-header title-header">
      <span class="title-text">{{ $firstCalendar->title }}</span>
    </header>
    <div class="box-body">
      @if ($firstCalendar->image)
        <img class="pull-left img-responsive img-article margin-r-5" src="{{ $firstCalendar->image->getRelativeUrl() }}" alt="{{ $firstCalendar->image->alt }}">
      @endif
      <span class="direct-chat-timestamp">Data:23 Jan 2:00 pm</span>
      <p class="text-justify">{!! $firstCalendar->body !!}</p>
    </div>
  </article>
</section>
<section class="row box box-solid no-padding">
  <header class="box-header title-header">
    <span class="title-text">Calendário</span>
  </header>
  <div class="box-body">
    <ul class="event-list">
      @foreach ($calendars as $calendar)
        <li>
          <time datetime="{{ $calendar->start_at }}">
            <span class="day">{{ $calendar->day }}</span>
            <span class="month">{{ $calendar->month }}</span>
            <span class="year">{{ $calendar->year }}</span>
            <span class="time">{{ $calendar->time }}</span>
          </time>
          <div class="info">
            <span class="direct-chat-timestamp title">Data final: 23 Jan 2:00 pm</span>
            <h4 class="title"><a href="{!! route('site.calendar', $calendar->slug) !!}">{{ $calendar->title }}</a></h4>
          </div>
        </li>
      @endforeach
    </ul>
  </div>
</section>
@stop
