@extends('site.layouts.main')

@section('title')
{{ $gallery->title }}
@stop

@section('content')
  <div class="box box-solid">
    <div class="box-header">
      <div class="box-title title-header">
        <span class="title-text">{{ $gallery->title }}</span>
      </div>
    </div>
    <div class="box-body">
      <div class="photos row">
        @foreach ($gallery->images as $image)
          <div class="photo-item col-xs-6 col-sm-4 col-md-3">
            <a class="fancybox" href="{!! $image->getRelativeUrl() !!}" rel="gallery">
              <img class="img-responsive cover-center" src="{!! $image->getRelativeUrl() !!}" alt="{{ $image->alt }}">
            </a>
          </div>
        @endforeach
      </div>
    </div>
  </div>
@stop
