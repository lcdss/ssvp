<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <base href="{!! asset('/') !!}">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <title>SSVP | Login</title>

  <link rel="stylesheet" href="css/app.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{!! route('site.index') !!}"><b>SSVP</b></a>
  </div>
  <div class="login-box-body">
    <p class="login-box-msg">Entre para iniciar sua sessão</p>
    @include('partials.errors')
    <form action="{{ route('auth.login.post') }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email" required>
        <span class="fa fa-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Senha" required>
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <input type="checkbox" name="remember"> Lembrar-me
        </div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-warning btn-block btn-flat">Entrar</button>
        </div>
      </div>
    </form>

    <a href="#">Esqueci minha senha</a><br>

  </div>
</div>

<script src="js/vendor.js"></script>
<script src="js/app.js"></script>
</body>
</html>
