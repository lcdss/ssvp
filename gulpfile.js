var elixir = require('laravel-elixir');
var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var autoprefixPlugin = new LessPluginAutoPrefix({browsers: ["last 2 versions"]});

elixir.config.css.less.pluginOptions = [autoprefixPlugin];

elixir(function (mix) {
    mix
        .copy('vendor/bower_components/AdminLTE/build', 'resources/assets/less/vendor/adminlte')
        .copy('vendor/bower_components/eonasdan-bootstrap-datetimepicker/src/less', 'resources/assets/less/vendor/bootstrap-datetimepicker')
        .copy('vendor/bower_components/font-awesome/less', 'resources/assets/less/vendor/font-awesome')
        .copy('vendor/bower_components/bootstrap/less', 'resources/assets/less/vendor/bootstrap')
        .copy('vendor/bower_components/AdminLTE/dist/css/skins', 'public/css/skins')
        .copy('vendor/bower_components/font-awesome/fonts', 'public/fonts')
        .styles([
            './vendor/bower_components/datatables/media/css/dataTables.bootstrap.css',
            './vendor/bower_components/medium-editor/dist/css/medium-editor.css',
            './vendor/bower_components/medium-editor/dist/css/themes/bootstrap.css',
            './vendor/bower_components/fancybox/source/jquery.fancybox.css',
        ], 'public/css/vendor.css')
        .scripts([
            './vendor/bower_components/jquery/dist/jquery.js',
            './vendor/bower_components/bootstrap/dist/js/bootstrap.js',
            './vendor/bower_components/moment/moment.js',
            './vendor/bower_components/moment/locale/pt-br.js',
            './vendor/bower_components/medium-editor/dist/js/medium-editor.js',
            './vendor/bower_components/datatables/media/js/jquery.dataTables.js',
            './vendor/bower_components/datatables/media/js/dataTables.bootstrap.js',
            './vendor/bower_components/jquery-mask-plugin/src/jquery.mask.js',
            './vendor/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            './vendor/bower_components/AdminLTE/dist/js/app.js',
            './vendor/bower_components/jquery-mousewheel/jquery.mousewheel.js',
            './vendor/bower_components/fancybox/source/jquery.fancybox.js',
        ], 'public/js/vendor.js')
        .scripts([
            'adminlte.js',
            'plugins.js',
            'app.js',
            'carousel.js',
        ], 'public/js/app.js')
        .less('app.less')
        .browserSync({
            notify: false,
            proxy: 'ssvp.dev',
        });
});
