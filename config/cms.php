<?php

return [
    'upload_path' => 'uploads',

    /**
     * CSS Skin
     *
     * Supported: 'black', 'blue', 'green', 'purple', 'red', 'yellow',
     *            'black-light', 'blue-light', 'green-light', 'purple-light',
     *            'red-light', 'yellow-light'
     */
    'skin' => 'yellow',
];
